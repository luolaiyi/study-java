package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class DemoAnnotation {
}

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
@interface Demo{

    int age() default 22;
    String value() default "沙雕";

}

class DemoAnno{
    String name;
    public void eat(){
        System.out.println("饿死了，想回家干饭");
    }
}