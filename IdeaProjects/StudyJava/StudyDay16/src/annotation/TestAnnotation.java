package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**本类用于测试自定义注解*/
public class TestAnnotation {  }

//1.2通过@Target注解表示此自定义注解可以使用的位置
/**2.注意@Target注解使用时需要导包
 * 通过ElementType.静态常量值来指定此自定义注解可以标记的元素
 * 如果有多个值,可以使用"{   ,  } "的格式来写:@Target({ElementType.METHOD,ElementType.FIELD,ElementType.TYPE})
 * */
//@Target({ElementType.METHOD,ElementType.FIELD,ElementType.TYPE})//表示此注解可以用来标记方法
@Target(ElementType.METHOD)//表示此注解可以用来标记方法
//1.3通过@Retention注解表示自定义注解的声明周期
/**3.注意@Retention使用时需要导包
 * 通过RetentionPolicy.静态常量值来指定此自定义注解的生命周期
 * 也就是指自定义注解可以存在在哪个文件中:源文件/字节码文件/运行时,只能3选1,不能写多个,因为源码:RetentionPolicy value();
 * */
@Retention(RetentionPolicy.SOURCE)
//首先注意:自定义注解的语法与java不同,不要套用java的语法
//1.1定义自定义注解,注解名是Test
/**1.注解定义需要使用"@interface 注解名" 的方式来定义*/
@interface Test{
    /**5.自定义注解还可以添加功能--给注解添加属性
     * 注意int age();不是方法的定义,而是给自定义注解中定义age属性的语法
     *如果为了使用注解时方便,还可以给age属性指定默认值,这样就可以直接使用,格式:"int age() default 0;"
     * */
    //int age();
    int age() default 0;//定义属性并赋值
    /**6.注解中还可以添加功能--可以定义特殊属性value
     * 特殊属性的定义方式与别的属性相同,主要是使用方式不同
     * 使用此注解给属性赋值的时候可以不用写成"@Test(value = "apple")",格式可以简化成"@Test("apple")",直接写值
     * 但是在自定义注解类中的赋予默认值不能简写,如果自定义了默认值,使用时可以不用赋值直接使用属性的默认值
     * */
    //String value();
    String value() default "lemon";//定义属性并赋值
    //可以定义特殊属性value，特殊属性的定义方式与普通属性相同，主要是使用方式不同，使用此注解给属性赋值的时候，可以不用
    //写成@Test(value="apple"),格式可以简化@Test("apple")直接写值
}

/**4.使用注解时,只要在指定的自定义注解名字前加上@即可使用此注解*/
//2.1自定义一个类用来测试自定义注解
//@Test
class TestAnno{
    /**测试1:给TestAnno类/name属性/eat()都添加了@Test注解,只有方法上不报错
     * 结论: 自定义注解能够加在什么位置,取决于@Target的值
     * 测试2:修改@Target的值@Target({ElementType.METHOD,ElementType.FIELD,ElementType.TYPE}),3个@Test都不再报错
     * 结论:注解@Test可以有多个位置,多个值,可以使用"{   ,  } "的格式,@Target中维护的是ElementType[]
     * */
    //@Test
    String name;
    /**测试3:添加了age属性以后,@Test注解报错
     * 结论:当注解没有定义属性时,可以直接使用,如果有属性了,就需要给属性赋值
     * 测试4:给@Test注解的age属性赋值以后,就不报错了
     * 结论:给属性赋值时的格式"@Test(age = 10)",注意,不能直接写10这种错误格式
     *测试5:给age属性赋予默认值后,还可以不加属性直接使用@Test注解,此时使用的就是age属性的默认值0
     * */
    /**测试6:给特殊属性value赋值时可以简写,相当于value="lemon"*/
    //@Test("lemon")
    @Test/**测试7:因为已有默认值,所以不用给特殊属性value赋值,直接使用@Test*/
    public void eat() {
        System.out.println("干饭啦!");
    }
}
