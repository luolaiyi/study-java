package Thread;

public class ExtendsThreadTest {
    public static void main(String[] args) {
        MyThread thread1=new MyThread();
        MyThread thread2=new MyThread();
        MyThread thread3=new MyThread();
        MyThread thread4=new MyThread();

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }
}
//自定义一个多线程类
class MyThread extends Thread{
    @Override
    public void run() {
        for (int i=1;i<=10;i++){
            System.out.println(getName()+"="+i);
        }
    }
}
