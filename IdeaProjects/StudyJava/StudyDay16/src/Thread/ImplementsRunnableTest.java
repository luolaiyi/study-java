package Thread;

public class ImplementsRunnableTest {
    public static void main(String[] args) {
        MyRunnable target=new MyRunnable();
        Thread thread1=new Thread(target);
        Thread thread2=new Thread(target);
        Thread thread3=new Thread(target);
        Thread thread4=new Thread(target);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

    }
}
class MyRunnable implements Runnable{

    @Override
    public void run() {
        for (int i=1;i<=10;i++){
            System.out.println(Thread.currentThread().getName()+"="+i);
        }
    }
}