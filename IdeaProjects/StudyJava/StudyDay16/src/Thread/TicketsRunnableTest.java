package Thread;
//这个类用来测试多线程售票
//需求:设计4个售票窗口，总计售票100张
//1、每次创建线程对象，都会生成一个tickets变量值是100，创建4次对象就生成了400张票了。
//不符合需求，怎么解决呢？能不能把tickets变量在每个对象间共享，就保证多少个对象都是卖这100张票。
//解决方案: 用静态修饰
//2、产生超卖，0 张 、-1张、-2张。
//3、产生重卖，同一张票卖给多人。
//4、多线程安全问题是如何出现的？常见情况是由于线程的随机性+访问延迟。
//5、以后如何判断程序有没有线程安全问题？
//在多线程程序中 + 有共享数据 + 多条语句操作共享数据

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TicketsRunnableTest {
    public static void main(String[] args) {
        //创建接口实现类对象作为目标对象(目标对象就是要做的业务)
        TicketsRunnable target = new TicketsRunnable();

//        Thread thread1 = new Thread(target);
//        Thread thread2 = new Thread(target);
//        Thread thread3 = new Thread(target);
//        Thread thread4 = new Thread(target);
//
//        thread1.start();
//        thread2.start();
//        thread3.start();
//        thread4.start();
        //创建线程池对象
        /**7.线程池ExecutorService:用来存储线程的池子,把新建线程/启动线程/关闭线程的任务都交给池来管理*/
        /**8.Executors用来辅助创建线程池对象,newFixedThreadPool()创建具有参数个数的线程数的线程池*/
        ExecutorService pool=Executors.newFixedThreadPool(5);
        for (int i=1;i<=5;i++){
            pool.execute(target);/**9.excute()让线程池中的线程来执行任务,每次调用都会启动一个线程*/
        }




    }
}
//1、在多线程程序中 + 2、有共享数据 + 3、多条语句操作共享数据
//在多线程程序中，有共享数据，多条语句操作共享数据

//同步锁:相当于给容易出现问题的代码加了一把锁,包裹了所有可能会出现问题的代码
//加锁范围:不能太大,也不能太小,太大,干啥都得排队,导致程序的效率太低,太小,没锁住,会有安全问题

class TicketsRunnable implements Runnable{
    static int tickets = 100;//静态成员变量
    Object object = new Object();//创建了一个唯一的"锁"对象,
    // 不论之后那个线程进同步代码块,使用的都是o对象,"唯一"很重要
    @Override
   //synchronized public void run() {//同步方法,如果一个方法中的所有代码都需要同步，
    // 那这个方法可以设置成同步方法
   public void run() {//同步方法
        //synchronized (object) {
        while (true) {
            /**4.synchronized(锁对象){}--同步代码块:是指同一时间这一资源会被一个线程独享,大家使用的时候,都得排队,同步效果*/
            /**5.锁对象的要求:多线程之间必须使用同一把锁,同步代码块的方式,关于锁对象可以任意定义*/
            //9.出现了线程安全问题,所以需要加同步代码块
            //9.1这种写法不对,相当于每个线程进来一次new一个锁对象,并不是使用的同一把锁
            //synchronized (new Object()) {
            //9.3 使用同步代码块,锁对象是o
            synchronized (object) {
            if (tickets>0) {
            try {
                //先让程序休眠10ms
                //问题1：超卖，0  -1
                //问题2：重卖，7号票卖给了两个人
                Thread.sleep(10);//程序休眠10ms
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //synchronized (object) {
            //if (tickets>0) {
                System.out.println(Thread.currentThread().getName() + " = " + tickets--);
            }
                if (tickets <= 0)
                    break;
            }
        }

    }
}
