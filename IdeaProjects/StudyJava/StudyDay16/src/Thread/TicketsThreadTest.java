package Thread;
/*
//这个类用来测试多线程售票,通过继承Thread类来实现案例
//需求:设计4个售票窗口，总计售票100张
 */
public class TicketsThreadTest {
    public static void main(String[] args) {
        //4.创建线程对象
        TicketsThread thread1=new TicketsThread();
        TicketsThread thread2=new TicketsThread();
        TicketsThread thread3=new TicketsThread();
        TicketsThread thread4=new TicketsThread();
        //问题1：我们想让程序只卖100张票，但是目前卖了400张，为什么？
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();


    }
}
class TicketsThread extends Thread{
    static int tickets=100;

    //int tickets=100,会卖400张票
    @Override
    public void run() {
        while (true) {
            synchronized (TicketsThread.class) {
                if (tickets>0) {
                    try {
                        //问题2：产生了重卖：同一张票卖给了多个人
                        //问题3：产生了超卖：超出了票数，甚至卖出了0和-1和-2
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //synchronized (TicketsThread.class) {
                    System.out.println(getName() + " = " + tickets--);
                }
                if (tickets <= 0)
                    break;
            }
        }
    }
}