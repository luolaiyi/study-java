package Singleton;
/*
懒汉式单例模式作业练习
 */
public class SingletonDemo2 {
    public static void main(String[] args) {
        SingleDemo2 singleDemo1=SingleDemo2.getSingleDemo2();
        SingleDemo2 singleDemo2=SingleDemo2.getSingleDemo2();
        System.out.println(singleDemo1);//Singleton.SingleDemo2@1540e19d
        System.out.println(singleDemo2);//Singleton.SingleDemo2@1540e19d
        System.out.println(singleDemo1==singleDemo2);//true
        System.out.println(singleDemo1.equals(singleDemo2));//true


    }
}
class SingleDemo2{
    //创建一个私有化的构造方法，防止外界通过构造方法实例化对象
    private SingleDemo2(){

    }
    //创建一个私有化的引用类型变量
    private static  SingleDemo2 singleDemo2;

    ////对外提供公共的全局访问点
    synchronized public static SingleDemo2 getSingleDemo2(){
        if (singleDemo2==null){
            singleDemo2=new SingleDemo2();
        }
        return singleDemo2;
    }
}

