package Singleton;
/**本类用于测试单例设计模式2-懒汉式--面试重点!!!*/
//总结:
//1.延迟加载的思想:是指不会在第一时间就把对象创建好来占用内存,而是什么时候需要用到,什么时候才去创建
//2.线程安全问题:是指共享资源有线程并发的数据隐患,可以通过加锁的方式[同步代码块/同步方法]
//饿汉式与懒汉式的区别:
//饿汉式是不管你用不用这个类的对象,先帮你创建一个
//懒汉式是先不给你创建这个类的对象,等你需要时再帮你创建--延迟加载的思想

public class SingletonTest2 {
    public static void main(String[] args) {
        MySingle2 mySingle1=MySingle2.getMySingle2();
        MySingle2 mySingle2=MySingle2.getMySingle2();
        System.out.println(mySingle1);
        System.out.println(mySingle2);
        System.out.println(mySingle1==mySingle2);
    }
}
class MySingle2 {
    //构造方法私有化，防止外界调用构造方法实例化对象
    private MySingle2() {

    }

    //在类的内部创建好引用类型变量（延迟加载思想）
    private static MySingle2 mySingle2;
    static Object object = new Object();

    //对外提供公共的全局访问点
    synchronized public static MySingle2 getMySingle2() {
        /**同步代码块:静态区域内,不能使用this关键字,因为静态资源优先于对象this加载
         * 因为静态资源属于类资源,随着类的加载而加载,而this指的是本类对象*/
        //7.解决线程安全问题
        //synchronized (this) {//报错:Cannot use this in a static context
        //synchronized (obj) {
        //4.当用户调用此方法时,才说明用到对象了,那么再将本类对象返回
        //当用户调用此方法时，才说明用到这个对象了，那么我们就把这个对象返回
            if (mySingle2 == null) {
                mySingle2 = new MySingle2();
            }
            return mySingle2;
        }
    }
//}