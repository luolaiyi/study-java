package Singleton;
/*
本类用于测试单例设计模式，饿汉式
 */
public class SingletonTest {
    public static void main(String[] args) {
        MySingle mySingle1 = MySingle.getMySingle();
        MySingle mySingle2 = MySingle.getMySingle();
        System.out.println(mySingle1);
        System.out.println(mySingle2);
        System.out.println(mySingle1==mySingle2);//true,==比较的是地址值，一个对象
        System.out.println(mySingle1.equals(mySingle2));//true
    }

}
class MySingle{
    //提供构造方法，并将构造方法私有化，为了不让外界随意实例化本类对象
    private MySingle(){

    }
    //创建私有化对象，本资源也需要使用static修饰，因为静态方法getMySingle()只能调用静态资源
    private static MySingle mySingle=new MySingle();

    //对外提供一个公共的全局访问点,用static修饰方法，为了外界可以通过类名调用方法
    /**
     * 3.思考:对象创建好并且用公共的方式返回,那么此公共方法外界该如何调用呢?
     * 之前我们都是在外部创建好本类对象进行调用的,但是现在外界无法创建本类对象
     * 解决方案:我们可以利用之前学习的静态的概念,被static关键字修饰的资源可以通过类名直接调用
     */

    public static MySingle getMySingle(){
        return  mySingle;
    }

}