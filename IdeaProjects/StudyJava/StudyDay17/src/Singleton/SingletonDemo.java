package Singleton;
/*
饿汉式单例作业练习
 */
public class SingletonDemo {
    public static void main(String[] args) {
       SingleDemo singleDemo1=SingleDemo.getSingleDemo();
       SingleDemo singleDemo2=SingleDemo.getSingleDemo();
        System.out.println(singleDemo1);//Singleton.SingleDemo@1540e19d
        System.out.println(singleDemo2);//Singleton.SingleDemo@1540e19d
        System.out.println(singleDemo1==singleDemo2);//true
        System.out.println(singleDemo1.equals(singleDemo2));//true
    }
}
class SingleDemo{
    //创建私有的构造方法，防止外界通过构造方法实例化对象
    private SingleDemo(){

    }
    //创建私有对象
    private static SingleDemo singleDemo=new SingleDemo();
    //对外提供一个公共的全局访问点,用static修饰方法，为了外界可以通过类名调用方法
    public static SingleDemo getSingleDemo(){
        return  singleDemo;
    }


}