package Reflection;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/*
Reflectionz作业练习
 */
public class ReflectionExer {
//    public static void main(String[] args) {
//
//    }
    @Test
    //Class.forName(“类的全路径”);
    //类名.class
    //对象.getClass();
    public void getClazz() throws ClassNotFoundException {
        Class<?> stud1=Class.forName("Reflection.StudentExer");//创建字节码对象
        Class<?> stud2=StudentExer.class;//创建字节码对象
        Class<?> stud3=new StudentExer().getClass();//创建字节码对象
        System.out.println(stud1);//反射得到的字节码Class对象
        System.out.println(stud2.getName());
        System.out.println(stud3.getSimpleName());
        System.out.println(stud3.getPackage().getName());
    }
    @Test
    public void getConstruct(){
        Class<?> stud4=StudentExer.class;//创建字节码对象
        Constructor<?>[] constructors = stud4.getConstructors();//获取构造方法们
        //遍历构造方法
        for (Constructor c:constructors){
            //获取构造方法名
            System.out.println(c.getName());
            Class<?>[] cc=c.getParameterTypes();//获取构造方法的参数类型,可能有多个
            System.out.println(Arrays.toString(cc));
        }
    }

    @Test
    public void getFunction(){
        Class<?> stud5=StudentExer.class;//创建字节码对象
        Method[] methods=stud5.getMethods();//获取所有的普通方法
        for (Method m:methods){
            System.out.println(m.getName());//获取普通方法名
            Class<?> [] mm = m.getParameterTypes();//获取普通方法参数类型
            System.out.println(Arrays.toString(mm));
        }
    }

    @Test
    public void getField(){
        Class<?> stud6=StudentExer.class;//创建字节码对象
        Field[] fields = stud6.getFields();
        for (Field f:fields){
            System.out.println(f.getName());
            System.out.println(f.getType().getName());
        }
    }
@Test
    public void getObject() throws Exception {
    Class<?> stud7=StudentExer.class;//创建字节码对象
    Object object = stud7.newInstance();
    System.out.println(object);
    Constructor<?> constructor=stud7.getConstructor(String.class,int.class);//获取构造器对象
    Object obj=constructor.newInstance("沙雕",22);
    System.out.println(obj);

    StudentExer stu=(StudentExer) obj;
    System.out.println(stu.age);
    System.out.println(stu.name);
    stu.study();


}

}
