package Reflection;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/*
测试反射
 */
public class ReflectionTest {
//    public static void main(String[] args) {
//
//    }
    //创建mian方法不用
    //单元测试方法：是JAVA测试的最小范围，使用灵活
    //语法要求: @Test + void + 没有参数 + public
    //注意使用时需要导包:Add JUnit 4 library to the build path:import org.junit.Test;
    //单元测试方法执行方式:选中方法名-->右键运行(Run As-->JUnit Test)-->出现小绿条说明执行成功
    //2.通过单元测试来测试如何获取类对象
    @Test
    public void getClazz() throws ClassNotFoundException {
        //1、通过类的全路径名获取对应的字节码对象
        Class<?> student1 = Class.forName("Reflection.Student");
        //2、直接类名.class
        Class<?> student2 = Student.class;
        //3、通过类创建的对象.getClass()
        Class<?> student3 = new Student().getClass();
        System.out.println(student1);
        System.out.println(student2.getName());//Reflection.Student,全路径名
        System.out.println(student3.getSimpleName());//Student，类名
        System.out.println(student3.getPackage().getName());//Reflection，包名

      }
      //单元测试二
    @Test
    public void getConstruct(){
        //直接类名.class，获取字节码
        Class<?> student4 = Student.class;
        //获取构造方法们（所有构造方法），将其放入constructor数组中
        Constructor<?>[] constructor = student4.getConstructors();
        //遍历构造方法，增强For循环
        for (Constructor c:constructor){
            System.out.println(c.getName());//获取构造方法的名称
           Class<?>[] cc=c.getParameterTypes();//获取构造方法的参数类型,可能有多个
            System.out.println(Arrays.toString(cc));
        }
    }

    @Test
    public void getFunction(){
        //直接类名.class，获取字节码对象
        Class<?> student5 = Student.class;
        //获取指定类中的所有普通方法
        Method[] methods=student5.getMethods();
        //遍历存放所有方法的数组
        for (Method m:methods){
            //通过当前遍历到的方法对象获取当前方法的名字
            System.out.println(m.getName());
            //通过当前遍历到的方法对象获取当前方法的参数类型
            Class<?>[] mm=m.getParameterTypes();
            System.out.println(Arrays.toString(mm));
        }
    }

    @Test
    public void getFields(){
        //直接类名.class，获取字节码对象
        Class<?> student6 = Student.class;
        //通过字节码对象获取指定类的成员变量
        ///**!!!注意目前成员变量的修饰符必须是public才能获取到,采用默认修饰符就反射不到*/
        Field[] fields=student6.getFields();
        //遍历存放所有成员变量的数组
        for (Field field:fields){
            //通过当前遍历到的方法对象获取当前成员变量的名字
            System.out.println(field.getName());
            //通过当前遍历到的方法对象获取当前成员变量的类型的名字
            System.out.println(field.getType().getName());
        }

    }

    @Test
    //* 方式一:通过字节码对象直接调用newInstance(),触发无参构造来创建对象
    //* 方式二:先获取指定的构造函数,再通过构造函数对象调用newInstance(),触发对应的构造函数来创建对象
    public void getObject() throws Exception {
        //直接类名.class，获取字节码对象
        Class<?> student7 = Student.class;
        //1、创建对象，触发无参构造
        Object object = student7.newInstance();
        System.out.println(object);//Student{name='null', age=0}
        //2、可以指定去调用哪个构造方法,注意根据参数来指定,而且传入的是参数的字节码对象
        //Constructor<?> cc = student7.getConstructor(String.class,int.class);//难度系数五颗星
        Constructor<?> cc = student7.getConstructor(String.class,int.class);

        Object obj=cc.newInstance("张三",18);//此处是多态的向上造型
        System.out.println(obj);
        //5.查看对象具体的属性值,或者调用方法,需要把Object强转成指定的子类类型
        /**为什么要把Object强转成子类类型?因为想要使用子类的特有功能,父类无法使用子类的特有功能
         * obj是Object类型,Object中没有Student的属性与功能
         * 这个操作叫做向下转型--想使用子类特有功能时需要做此操作
         **/
        Student stu=(Student) obj;//向下转型
        System.out.println(stu.name);
        System.out.println(stu.age);
        stu.eat();

    }
}
