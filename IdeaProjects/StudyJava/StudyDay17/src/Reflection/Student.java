package Reflection;
/*
创建一个物料类，测试反射
 */
public class Student {
    //创建属性
    public String name;
    public int age;
    //创建无参构造方法
    public Student(){
        System.out.println("我是一个没有感情的无参构造方法");
    }
    //创建全参构造方法
    public Student(String name, int age) {
        System.out.println("我是一个没有感情的全参构造方法");
        this.name = name;
        this.age = age;
    }
    public void eat(){
        System.out.println("猫吃鱼，狗吃肉，奥特曼打小怪兽");
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
