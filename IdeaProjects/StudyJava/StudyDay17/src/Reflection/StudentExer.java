package Reflection;
/*
Reflection作业练习
 */
public class StudentExer {
    public String name;
    public int age;

    public StudentExer() {
        System.out.println("我是一个没有感情的无参构造函数");
    }

    public StudentExer(String name, int age) {
        System.out.println("我是一个没有感情的全参构造函数");
        this.name = name;
        this.age = age;
    }
    public void study(){
        System.out.println("我要成为JAVA开发大神");
    }

    @Override
    public String toString() {
        return "StudentExer{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
