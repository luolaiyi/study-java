package ApiDemo;


/*本类用于测试String类的用法*/
public class TestString {
    public static void main(String[] args) {
        //1.创建String对象方式一
        /*1.字符串底层维护的是char[].存放在堆中*/
        char[] value = {'a','b','c','a','d'};
        String s1 = new String(value);
        String s11 = "abccadb";
        System.out.println(s1.charAt(0));
        System.out.println(s1.concat("a"));
        System.out.println(s1.length());
        System.out.println(s1.equals(s11));//false
        System.out.println(s1.toUpperCase());
        System.out.println(String.valueOf(10)+10);
        //2.创建String对象方式二
        /*2.此种方式创建,底层也会new String(),存放在堆中常量池,效率高*/
        String s2 = "abc";
        String s22 =  "abc";
        //3.测试
        System.out.println("*****************************************");
        /*3.==比较的是地址值
        Object中equals()的默认实现比较的也是==地址值
        但String类对此方法做了重写,比较的就是字符串的具体内容*/
        System.out.println(s1 == s2);//false
        System.out.println(s1.equals(s2));//true
        System.out.println(s1 == s11);//false,没有高效效果
        System.out.println(s2 == s22);//true,有高效的效果
    }
}

