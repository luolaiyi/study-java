package ApiDemo;

import java.util.Objects;

public class ObjectTest {
    public static void main(String[] args) {
        Student student=new Student();
        Student student1=new Student("泡泡老师",18,'女');
        Student student2=new Student("泡泡老师",22,'女');
        System.out.println(student);
        System.out.println(student1);
        System.out.println(student2);
        System.out.println(student1.equals(student2));//true
        System.out.println(student.hashCode());
    }
}

class Student{
    String name;
    int age;
    char sex;

    public Student() {
        System.out.println("无参构造");
    }

    public Student(String name, int age, char sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        System.out.println("全参构造");
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return
                //age == student.age &&
                //sex == student.sex &&
                Objects.equals(name, student.name);
    }

}