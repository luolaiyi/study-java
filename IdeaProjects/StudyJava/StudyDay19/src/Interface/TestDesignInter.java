package Interface;


/**本类用于改造老师设计案例,采用面向接口编程*/
public class TestDesignInter {
    public static void main(String[] args) {
        CGBTeacher2 ct = new CGBTeacher2();
        ct.ready();
        ct.teach();
    }
}
//1.创建接口Teacher2--抽取共性,形成抽象层-体现接口-定义的是规则
/**1.通过interface关键字定义接口*/
interface Teacher2{
    /**2.接口中的方法都是抽象方法,可以简写public abstract*/
    //2.定义接口中的抽象方法
    //2.1备课方法
    void ready();
    //2.2上课方法
    void teach();
}
/**3.如果实现类想要使用接口的功能,就需要与接口建立实现关系*/
//3.创建接口的实现类并添加所有未实现的方法
class CGBTeacher2 implements Teacher2{
    @Override
    public void ready() {
        System.out.println("正在备课...电商项目");
    }
    @Override
    public void teach() {
        System.out.println("正在上课...电商项目");
    }
}
//4.创建接口的抽象子类
abstract class SCDTeacher2 implements Teacher2{}

//5.创建接口的抽象子类2
abstract class ACTTeacher2 implements Teacher2{
    @Override
    public void ready() {
        System.out.println("正在备课...基础加强..框架加强..高新技术");
    }
    public abstract void teach() ;
}
