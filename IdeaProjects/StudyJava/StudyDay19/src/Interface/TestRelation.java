package Interface;


/**本类用于测试接口与类之间的复杂关系*/
public class TestRelation {
    //5.创建入口函数
    public static void main(String[] args) {
        //6.创建实现类对象进行测试
        Innter3Impl i3 = new Innter3Impl();
        i3.update();
        i3.delete();
        //7.创建多态对象进行测试
        Innter3 i4 = new Innter3Impl();
        i4.find();
        i4.save();
    }
}
//1.创建接口1
interface Innter1{
    void save();//保存功能
    void delete();//删除功能
}
//2.创建接口2
interface Innter2{
    void update();//更新功能
    void find();//查询功能
}
//3.创建接口3用来测试接口与接口的继承关系
/**1.接口之间可以建立继承关系,而且还可以多继承
 * 接口与接口之间用逗号隔开
 * */
interface Innter3 extends Innter1,Innter2{

}
//4.创建Inter3接口的实现类并添加未实现的方法
/**2.接口和实现类之间可以建立实现关系,通过implments关键字来完成
 * 注意,java类是单继承,而接口不限,写接口时,我们一般先继承再实现
 * */
class Innter3Impl extends Object implements Innter3{//extends Object
    @Override
    public void save() {
        System.out.println("稍等...正在保存中...");
    }
    @Override
    public void delete() {
        System.out.println("稍等...正在删除中....");
    }
    @Override
    public void update() {
        System.out.println("客官，马上就更新好啦~~");
    }
    @Override
    public void find() {
        System.out.println("小二正在马不停蹄的查询~~~");
    }
}
