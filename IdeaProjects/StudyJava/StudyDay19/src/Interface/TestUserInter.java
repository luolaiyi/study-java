package Interface;
/**
 * 本类用于进一步测试接口的使用
 * 总结:接口里是没有构造方法的
 * 在创建实现类的对象时默认的super()，是调用的默认Object的无参构造
 * */
public class TestUserInter {
    //5.创建入口函数main()
    public static void main(String[] args) {
        //6.创建多态对象进行测试
        /**问题:子类创建对象时,默认会调用父类的构造方法
         * 目前接口实现类的父级是一个接口,而接口没有构造方法
         * 那实现类构造方法中的super()调用的是谁呢?
         * 结论:如果一个类没有明确指定父类,那么默认继承顶级父类Object
         * 所以super()会自动调用Object类中的无参构造
         * */
        /**查看类的继承结构:Ctrl+T*/
        Inter2 inter2 = new Inter2Impl();
        System.out.println(Inter2.num);//接口中的变量实际上都是静态常量,可以通过类名直接调用
        //Inter2.num=200;接口中的变量实际上都是静态常量,值不可以被修改
        inter2.eat();
        inter2.study();
    }
}

//1.创建接口
interface Inter2{
    /**1.接口中有构造方法吗?--不可以!!!*/
    //2.测试接口是否可以有构造方法
    //public Inter2() {}
    public static final int age = 10;
    int num=100;//静态常量
    abstract public void eat();
    void study();



}

//3.创建接口的实现类
//class Inter2Impl extends Object implements Inter2{
class Inter2Impl implements Inter2{
    //4.创建实现类的构造函数
    public Inter2Impl() {
        super();//extends Object
        System.out.println("我是Inter2Impl的无参构造");
    }

    @Override
    public void eat() {
        System.out.println("吃饭饭");
    }

    @Override
    public void study() {
        System.out.println("学习习");
    }
}
