package Interface;
/*
通过interface关键字来定义接口
通过implements让子类来实现接口
接口中的方法全部都是抽象方法(JAVA8)
可以把接口理解成一个特殊的抽象类(但接口不是类!!!)
类描述的是一类事物的属性和方法,接口则是包含实现类要实现的方法
接口突破了java单继承的局限性
接口和类之间可以多实现,接口与接口之间可以多继承
接口是对外暴露的规则,是一套开发规范
接口提高了程序的功能拓展,降低了耦合性
 */
public class InterfaceTest {
    public static void main(String[] args) {
      Inner inner=new ImpInner();
      inner.eat();
      inner.study();

      ImpInner impInner=new ImpInner();
      impInner.eat();
      impInner.study();
    }
}
interface Inner{
    abstract public void eat();//接口中都是抽象方法，没有方法体
    abstract public void study();//接口中都是抽象方法，没有方法体
}

class ImpInner implements Inner{

    @Override
    public void eat() {
        System.out.println("接口实现类方法，好饿啊，我想去干饭了");
    }

    @Override
    public void study() {
        System.out.println("接口实现类方法，好饿啊，我想去学习了");
    }
}