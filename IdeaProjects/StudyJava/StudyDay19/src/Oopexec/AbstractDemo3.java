package Oopexec;

/**本类用于测试抽象类中的成员*/
public class AbstractDemo3 {
    public static void main(String[] args) {
        //7.创建多态对象并进行测试
        Fruit f = new Banana();
        System.out.println(f.sum);
        //f.name = "ZHR";//常量的值不可以被修改
        System.out.println(f.name);
        f.clean();//调用父类自己的功能
        f.grow();//调用子类实现的功能
    }
}

//1.创建抽象父类水果类--Fruit
abstract class Fruit{
    /**1.抽象类中可以有成员变量吗?--可以!!!*/
    //3.1创建抽象父类的成员变量
    int sum = 100;
    /**2.抽象类中可以有成员常量吗?--可以!!!*/
    //3.2创建抽象父类中的成员常量,注意要初始化
    final String name ="XIAOHUANGREN";

    /**3.抽象类中可以有普通方法吗?--可以!!!
     * 抽象类中可以都是普通方法吗?--可以!!!*/
    /**4.如果一个类中都是普通方法,为啥还要被声明抽象类呢?
     * 原因:抽象类不可以创建对象
     * 所以如果不想让外界创建本类的对象,就可以把普通类声明成抽象类*/
    //4.创建抽象父类的普通方法
    public void clean() {
        System.out.println("吃水果之前记得洗");
    }

    //5.创建抽象父类中的抽象方法
    /**5.抽象类中可以有抽象方法,一旦类中有抽象方法
     * 这个类必须被声明成一个抽象类*/
    public abstract void grow();
    public abstract void clean2();
}
//2.创建子类Banana
/**6.当一个类继承了父类,并且父类是抽象父类时
 * 子类需要重写(实现)父类中的所有抽象方法或者把自己变成抽象子类*/
//6.1 解决方案一:把自己变成一个抽象子类
//abstract class Banana extends Fruit{
//6.2 解决方案二:实现继承自抽象父类的所有抽象方法
class Banana extends Fruit{
    @Override
    public void grow() {
        System.out.println("一串香蕉老沉啦~");
    }
    @Override
    public void clean2() {
        System.out.println("香蕉不用洗,香蕉想被扒皮");
    }
}
