package Oopexec;
/*
抽象类内容复习
如果一个类含有抽象方法,那么它一定是抽象类
抽象类中的方法实现交给子类来完成
abstract 可以修饰方法或者类
被abstarct修饰的类叫做抽象类,被abstract修饰的方法叫做抽象方法
抽象类中可以没有抽象方法
如果类中有抽象方法,那么该类必须定义为一个抽象类
子类继承了抽象类以后,要么还是一个抽象类,要么就把父类的所有抽象方法都重写
多用于多态中
抽象类不可以被实例化

 */
public class AbstractDemo {
    public static void main(String[] args) {
        Animal animal=new Dog();
        animal.eat();
        animal.study();

    }
}
abstract class Animal{
   public void eat(){
       System.out.println("抽象类普通方法，干饭去");
   }
   public void sleep(){
       System.out.println("抽象类普通方法，睡觉去");
   }
   abstract public void study();//抽象方法没有方法体
    abstract public void learn();//抽象方法没有方法体

}

class Dog extends Animal{

    @Override
    public void study() {
        System.out.println("子类重写抽象父类");
    }

    @Override
    public void learn() {
        System.out.println("子类重写抽象父类");
    }
}