package Oopexec;
/*
多态内容复习
 */
public class DesignCar {
    public static void main(String[] args) {
        Car car=new Car();
        System.out.println(car.getColor());
        car.start();
        car.stop();
        System.out.println("*****************************************");

        BMW bmw=new BMW();
        System.out.println(bmw.getColor());//父类，颜色,使用的是父类的属性
        bmw.start();
        System.out.println(bmw.color);
        System.out.println("******************************************");

        AD ad=new AD();
        System.out.println(ad.color);
        ad.stop();
        System.out.println(ad.getColor());//父类，颜色,使用的是父类的属性
        System.out.println(ad.getBrand());

    }
}
 class Car{
     private String brand="父类，品牌";//品牌
     private String color="父类，颜色";

     public String getBrand() {
         return brand;
     }

     public void setBrand(String brand) {
         this.brand = brand;
     }

     public String getColor() {
         return color;
     }

     public void setColor(String color) {
         this.color = color;
     }

     public void start(){
         System.out.println("父类，我的汽车要启动了，想念泡泡老师的第一天");
     }
     public void stop(){
         System.out.println("父类，我的汽车要停止了");
     }
 }
 class BMW extends Car{
    String color="宝马子类，五彩斑斓的黑";

     public void start(){
         super.start();//使用父类方法
         System.out.println("宝马子类，我是宝马，我是最流弊的");
     }
 }

 class AD extends Car{
    String color="奥迪子类，黑的五彩斑斓";
    public void stop(){
        System.out.println("奥迪子类，我是奥迪，我怕谁");
    }
 }