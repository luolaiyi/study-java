package Oopexec;
/*
 * 1.抽象类中可以有构造方法
 * 2.父类的构造方法要优先于子类执行
 * 3.抽象类不可以创建对象(实例化)
 * 4.抽象类中存在的构造方法不是为了创建 本类 对象时调用而是为了创建 子类 对象时使用
 */
public class AbstractDemo2 {
    public static void main(String[] args) {
        Animal2 animal2=new Dog2();

    }
}
abstract class Animal2{
    public Animal2(){
        //super();
        System.out.println("我是抽象父类的无参构造方法");
    }
}

class Dog2 extends Animal2{
    public Dog2(){
        System.out.println("我是子类的无参构造方法");
    }
}