package ReaderTest;

import java.io.*;

/*
本类用于测试字符流
1.流只能单方向流动
2.输入流用来读取 → in
3.输出流用来写出 → out
4.数据只能从头到尾顺序的读写一次
 */
public class InnTest {
    public static void main(String[] args) {
        //method1();
        method2();

    }

    //普通字符流读取
    private static void method1(){
        //Reader reader1 = new FileReade(new File("D:\ready\1.txt"));
        try {
            Reader reader1 = new FileReader("D:\\ready\\1.txt");
            int num;
            while ((num=reader1.read())!=-1){
                System.out.println(num);

            }
            //关闭
            reader1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //高效字符流读取
    private static  void method2(){
        try {
            //Reader reader2 = new BufferedReader(new FileReader(new File("D:\\ready\\1.txt")));
            Reader reader2 = new BufferedReader(new FileReader("D:\\ready\\1.txt"));
            int num;
            while ((num=reader2.read())!=-1){//reader2.read()每次执行读取下一个字符
                System.out.println(num);
            }
            //关闭
            reader2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
