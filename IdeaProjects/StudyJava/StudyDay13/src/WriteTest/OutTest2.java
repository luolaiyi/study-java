package WriteTest;

import java.io.*;

/*
本类用于测试输出字符流
 */
public class OutTest2 {
    public static void main(String[] args) {
        method1();//普通输出方法
        method2();//高效输出方法
    }
    //普通输出方法
    private static void method1(){
        Writer writer1 = null;
        try {
            //Writer writer1 = new FileWriter(new File("D:\\ready\\3.txt"));
            //1、创建输出流对象
            //writer1 = new FileWriter("D:\\ready\\3.txt");
            writer1 = new FileWriter("D:\\ready\\4.txt");
            //2、使用输出流
            writer1.write(97);//a
            writer1.write(98);//b
            writer1.write(99);//c
            writer1.write(100);//d
            writer1.write(101);//e

            writer1.flush();//2.1、刷新输出流数据

            //3、关闭输出流
            //writer1.close();

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writer1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    //高效输出方法
    private static void method2(){
        Writer writer2 = null;
        try {//可能会出现异常的代码
            //1、创建输出流对象
            //Writer writer2 = new BufferedWriter(new FileWriter(new File("D:\\ready\\3.txt")));
            //writer2 = new BufferedWriter(new FileWriter("D:\\ready\\4.txt"));
            writer2 = new BufferedWriter(new FileWriter("D:\\ready\\4.txt",true));
            //2、使用输出流对象
            writer2.write(65);//A
            writer2.write(66);//B
            writer2.write(67);//C
            writer2.write(68);//D
            writer2.write(69);//E

            writer2.flush();//刷新输出流数据
            //3、关闭输出流
            //writer2.close();


        } catch (IOException e) {//捕获异常
            e.printStackTrace();
        }finally {//本结构中一定会执行到的代码块，必要操作，比如关闭流就可以放到这里
            try {
                writer2.close();//3、关闭输出流
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
