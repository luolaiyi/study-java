package WriteTest;

import java.io.*;
import java.util.Scanner;

/*
本类用于文件复制案例
需求：接收用户输入的文件路径来进行复制，复制到用户指定的路径下
 */
public abstract class CopyFileTest {
    public static void main(String[] args) {
        System.out.println("请输入原文件的路径");//复制谁
        String f = new Scanner(System.in).nextLine();
        System.out.println("请输入目标文件的路径");//复制到哪
        String t = new Scanner(System.in).nextLine();
        
        //吧接收到的路径进行封装，变成可以进行文件操作的File对象
        File from=new File(f);
        File to=new File(t);
        //调用方法进行复制
        ZJCopy(from,to);
        //ZFCopy(from,to);
    }

    public static void ZFCopy(File from, File to) {
        Reader in = null;//定义在整个方法中都生效的字符输入流对象,注意是局部变量,需要初始化,对象的默认值是null
        Writer out = null;//定义在整个方法中都生效的字符输出流对象,注意是局部变量,需要初始化,对象的默认值是null

        try {
            //1、通过输入流读取from,获取字符输入对象
            in =new BufferedReader(new FileReader(from));
            //2、通过输出流to，获取字符输出对象
            out =new BufferedWriter(new FileWriter(to));
            //3、边读边写，定义变量用来保存读到的数据
            int num = 0;
            while ((num = in.read()) != -1){
                out.write(num);//将读到的数据写出到文件
            }
            System.out.println("恭喜您!文件复制成功!");
        }catch (Exception e){
            System.out.println("很抱歉!文件复制失败!");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    public static void ZJCopy(File from, File to) {
        InputStream in = null;//定义在整个方法中都生效的字节输入流对象,注意是局部变量,需要初始化,对象的默认值是null
        OutputStream out = null;//定义在整个方法中都生效的字节输出流对象,注意是局部变量,需要初始化,对象的默认值是null
        try {
            //1.读取from文件--操作文件的是字节输入流
            in = new BufferedInputStream(new FileInputStream(from));
            //2.写出到to文件--操作文件的是字节输出流
            out = new BufferedOutputStream(new FileOutputStream(to));
            //3.边读边写
            int num = 0;
            while ((num=in.read())!=-1){
                out.write(num);
            }
            System.out.println("恭喜您!文件复制成功!");

        }catch (Exception e){
            System.out.println("很抱歉!文件复制失败!");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


}
