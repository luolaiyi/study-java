package WriteTest;

import java.io.*;

/*
本类用于测试字节输出流
 */
public class OutTest1 {
    public static void main(String[] args) {
        //method1();//普通输出流
        method2();//高效输出流
    }
    //普通输出流
    private static void method1(){
        OutputStream out1= null;
        try {
            //OutputStream out1 = new FileOutputStream(new File("D:\\ready\\2.txt"));
            //1、创建字节输出流对象
            out1 = new FileOutputStream("D:\\ready\\2.txt");
            //2、使用输出流写数据
            out1.write(6);
            out1.write(7);
            out1.write(8);
            out1.write(9);

            out1.flush();//刷新一下输出流
            //3、关闭流
            //out1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //如果要保证代码一定能执行，就通通放在finally代码块中
            try {//关闭流
                out1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    //高效输出流
    private static void method2(){
        OutputStream out2 = null;
        try {
            //OutputStream out2 = new BufferedOutputStream(new FileOutputStream(new File("D:\\ready\\2.txt")));
            //1、创建字节流输出对象
             out2 = new BufferedOutputStream(new FileOutputStream("D:\\ready\\2.txt"));
            //2、使用输出字节流
            out2.write(97);
            out2.write(98);
            out2.write(99);
            out2.write(100);
            //out2.close();
            out2.flush();//刷新
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //3、关闭输出字节流
            try {
                out2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
