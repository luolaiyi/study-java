package WriteTest;

import java.io.*;
import java.util.Scanner;

public class CopyFileTest2 {
    public static void main(String[] args) {
        //1.提示并接收用户输入的要复制的源文件路径--复制啥
        System.out.println("请输入要复制的原文件路径：");
        String f = new Scanner(System.in).nextLine();

        //2.提示并接收用户输入的要复制的目标文件路径--复制啥
        System.out.println("请输入要复制到的目标文件路径：");
        String t = new Scanner(System.in).nextLine();

        //3.1根据源文件路径封装from文件对象
        File from = new File(f);
        //3.2根据目标文件路径封装to文件对象
        File to = new File(t);

        //创建方法
        ZJcopy(from,to);
        //ZFcopy(from,to);
    }

    private static void ZFcopy(File from, File to) {
        Reader in= null;//定义在整个方法中都生效的字符输入流对象,注意是局部变量,需要初始化,对象的默认值是null
        Writer out= null;//定义在整个方法中都生效的字符输出流对象,注意是局部变量,需要初始化,对象的默认值是null
        try {
            //1.读取from文件--获取字符流输入对象
            in = new BufferedReader(new FileReader(from));
            //2.写出到to文件中--获取字符流输出对象
            out = new BufferedWriter(new FileWriter(to));

            int num = 0;
            while ((num=in.read())!=-1){//从原文件中读取字符数据
                out.write(num);//将原文件中读取的字符数据写到目标文件中
            }
            System.out.println("恭喜您!文件复制完成!");
        }catch (Exception e){
            System.out.println("温馨提示!文件复制失败!");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static void ZJcopy(File from, File to) {
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new BufferedInputStream(new FileInputStream(from));
            out = new BufferedOutputStream(new FileOutputStream(to));

            int num = 0;
            while ((num=in.read())!=-1){
                out.write(num);
            }
            System.out.println("恭喜您！文件复制完成！");
        }catch (Exception e){
            System.out.println("温馨提示！文件复制失败！");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
