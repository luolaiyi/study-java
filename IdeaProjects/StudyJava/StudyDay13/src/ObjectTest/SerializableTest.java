package ObjectTest;

import java.io.*;

/*

 */
public class SerializableTest {
    public static void main(String[] args) {
        method1();//本方法用来测试序列化
        method2();//本方法用来测试反序列化
    }
    //本方法用来测试序列化
    private static void method1() {
        //声明在本方法内都生效的序列化局部变量,局部变量需要初始化,默认值是null
        ObjectOutputStream out = null;
        try {
            //1.创建ObjectOutputStream流对象来完成序列化
            out = new ObjectOutputStream(new FileOutputStream("D:\\ready\\2.txt"));

            //创建Student对象,全参构造方法
            Student obj = new Student(164035,"罗来意","安徽大学江淮学院",'男');
            out.writeObject(obj);
            System.out.println(obj);
            System.out.println("恭喜您！序列化成功");

        }catch (Exception e){
            System.out.println("很抱歉!序列化失败!");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
    //本方法用来测试反序列化
    private static void method2() {
        //声明在本方法内都生效的反序列化局部变量,局部变量需要初始化,默认值是null
        ObjectInputStream in = null;
        try {
            //1.创建ObjectInputStream流对象来完成反序列化
            in = new ObjectInputStream(new FileInputStream("D:\\ready\\2.txt"));
            Object obj= in.readObject();//返回值类型Object
            System.out.println(obj);
            System.out.println("恭喜您！反序列化成功");
        }catch (Exception e){
            System.out.println("很抱歉!反序列化失败!");
            e.printStackTrace();
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
