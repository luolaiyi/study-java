package ObjectTest;

import java.io.*;

/**本类用于序列化与反序列化的测试类*/
//序列化:是指把程序中的java对象,永久保存在磁盘中,相当于是写出的过程,方向是out-->ObjectOutputStream
//反序列化:是指把已经序列化在文件中保存的数据,读取/恢复到java程序中的过程,方向是in-->ObjectInputStream

public class SerializableDemo {
    public static void main(String[] args) {
        method1();
        method2();

    }
    //序列化实现
    private static void method1() {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream("D:\\ready\\2.txt"));
            Student student=new Student(164035,"龙傲天","安徽大学",'男');
            out.writeObject(student);
            System.out.println(student);
            System.out.println("序列化成功");
        }catch (Exception e){
            System.out.println("序列化失败");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static void method2() {
        ObjectInputStream in=null;
        try {
            in=new ObjectInputStream(new FileInputStream("D:\\ready\\2.txt"));
            Object obj=in.readObject();
            System.out.println(obj);
            System.out.println("反序列化成功");
        }catch (Exception e){
            System.out.println("反序列化失败");
            e.printStackTrace();
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
