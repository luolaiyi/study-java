package ObjectTest;

import java.io.Serializable;

/*
 本类用来封装学生类
 * 如果本类想要完成序列化,必须实现可序列化接口,否则会报错:
 * 报错信息:java.io.NotSerializableException: ObjectTest.Student
 * Serializable接口是一个空接口,里面一个方法都没有,作用是用来当做标志,标志这个类可以序列化/反序列化
   Serializable是一个空接口，里面一个方法都没有，作用：当做标志，标志着这个类可以被序列化
 */
public class Student implements Serializable {
    /**需要给每个进行序列化的文件分配唯一的UID值*/
    //The serializable class Student does not declare a static final serialVersionUID field of type long
    //private static final long serialVersionUID = 1L;
    //private static final long serialVersionUID=1l;
    //1、定义属性
    private int sno;//学号
    private String name;//姓名
    private String address;//地址
    private char gender;//性别

    public Student() {//无参构造方法
    }
    //全参构造方法
    public Student(int sno, String name, String address, char gender) {
        this.sno = sno;
        this.name = name;
        this.address = address;
        this.gender = gender;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }
     @Override
     public String toString() {
         return "Student{" +
                 "sno=" + sno +
                 ", name='" + name + '\'' +
                 ", address='" + address + '\'' +
                 ", gender=" + gender +
                 '}';
     }


}
