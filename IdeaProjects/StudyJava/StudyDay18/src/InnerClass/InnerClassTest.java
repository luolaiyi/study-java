package InnerClass;
/*
1) 内部类可以直接访问外部类中的成员，包括私有成员
2) 外部类要访问内部类的成员，必须要建立内部类的对象
3) 在成员位置的内部类是成员内部类
4) 在局部位置的内部类是局部内部类
 */
public class InnerClassTest {
    public static void main(String[] args) {
        //外部类名.内部类名 变量名 = 外部类对象.内部类对象
        Outer.Inner inner = new Outer().new Inner();
        inner.delete();
        System.out.println(inner.sum);
        //调用外部类方法
        new Outer().find();

    }
}
//创建外部类
class Outer{
    private String name;
    private int age;

    public void find(){
        System.out.println("我是一个没有感情的外部类成员方法");
        Inner inn=new Inner();
        inn.delete();
        System.out.println(inn.sum);
    }
    //创建内部类，成员内部类（类里方法外），局部内部类（方法里，局部代码块）
    class Inner{
        int sum = 10;

        public void delete(){
            System.out.println("我是一个没有感情的内部类成员方法");
            //外部想使用内部类资源，必须先创建内部类对象，然后通过内部类对象来获取内部类资源
            System.out.println(name);
            System.out.println(age);
            //find();//相互调用，会形成死循环
        }
    }
}