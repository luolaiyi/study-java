package InnerClass;
/*
成员内部类被private修饰,无法被外界直接创建对象使用，所以可以创建外部类对象，通过外部类对象间接访问内部类的资源
1) 内部类可以直接访问外部类中的成员，包括私有成员
2) 外部类要访问内部类的成员，必须要建立内部类的对象
3) 在成员位置的内部类是成员内部类
4) 在局部位置的内部类是局部内部类
 */
public class InnerClassTest2 {
    public static void main(String[] args) {
        //外部类名.内部类名 变量名 = 外部类对象.内部类对象
//        Outer2.Inner2 inner2 = new Outer2().new Inner2();
//        inner2.eat();
        new Outer2().getInner2Eat();

    }
}
//创建外部类
class Outer2{
    //提供外部类公共的方法,在方法内部创建Inner2内部类对象,调用内部类方法
    public void getInner2Eat(){
        Inner2 inner2=new Inner2();
        inner2.eat();
    }
    //创建私有的内部类{
    private class Inner2{
        //创建成员内部类方法
        public void eat(){
            System.out.println("我是一个没有感情的内部类成员方法，我好饿啊，我想干饭");
        }
    }
}