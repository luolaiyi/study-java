package InnerClass;
/*
本类用来测试成员内部类被static修饰
1) 内部类可以直接访问外部类中的成员，包括私有成员
2) 外部类要访问内部类的成员，必须要建立内部类的对象
3) 在成员位置的内部类是成员内部类
4) 在局部位置的内部类是局部内部类
静态资源访问时不需要创建对象,可以通过类名直接访问
访问静态类中的静态资源可以通过”. . . ”链式加载的方式访问
 */
public class InnerClassTest3 {
    public static void main(String[] args) {
//        Outer3.Inner3 inner3=new Outer3().new Inner3();
//        inner3.show();
//        new Outer3().new Inner3().show();
        Outer3.Inner3 inner3 = new Outer3.Inner3();
        inner3.show();
        new Outer3.Inner3().show();
        Outer3.Inner3.showw();


    }
}
class Outer3{
    static class Inner3{
        public void show(){
            System.out.println("请开始你的表演");
        }
        public static void showw(){
            System.out.println("请继续你的表演");
        }
    }
}