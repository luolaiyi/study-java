package InnerClass;

/*
本类用来测试局部内部类
1) 内部类可以直接访问外部类中的成员，包括私有成员
2) 外部类要访问内部类的成员，必须要建立内部类的对象
3) 在成员位置的内部类是成员内部类
4) 在局部位置的内部类是局部内部类

 */
public class InnerClassTest4 {
    public static void main(String[] args) {
        //当在外部类show()中创建局部内部类对象并且进行功能调用后,内部类的功能才能被调用
        new Outer4().show();
    }
}
class Outer4{
    public void show(){
        //创建局部代码块
        class Inner4{
            String name;
            int age;

            public void bye(){
                System.out.println("泡泡老师已经准备跑路了");
            }
        }
        //在局部方法里创建局部类对象
        Inner4 inner4=new Inner4();
        System.out.println(inner4.age);
        System.out.println(inner4.name);
        inner4.bye();
    }
}