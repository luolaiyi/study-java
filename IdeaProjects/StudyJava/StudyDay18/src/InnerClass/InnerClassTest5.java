package InnerClass;
/*
本类用来测试匿名内部类,没有名字,通常结合着匿名对象一起使用
抽象类不能创建对象，但是有构造方法
 */
public class InnerClassTest5 {
    public static void main(String[] args) {
        //接口不能创建对象
        //就相当于创建了一个接口的实现类 + 重写接口中的所有抽象方法
        new Inner1() {
            @Override
            public void save() {
                System.out.println("我是Inner1接口的save()");
            }

            @Override
            public void get() {
                System.out.println("我是Inner1接口的get()方法");
            }
        }.get();//触发指定的重写后的方法,只能调用一个,调用一次
        /**注意!!!匿名对象只干一件事!!!*/

       new Inner2() {//new Inner2()匿名对象,相当于创建了抽象类的子类,必须重写所有抽象方法
           @Override
           public void drink() {
               System.out.println("我想喝可乐，但是我不说，就是玩儿...");
           }
       }.drink();

       new Inner3().eat();//普通类的匿名对象,没有强制要求产生匿名内部类的重写方法
        //如果使用对象,只需要干一件事--直接创建匿名对象,简单方便
       Inner3 inner3=new Inner3();//如果使用同一个对象,要干很多事情--必须给对象起名字
       inner3.eat();
       inner3.sleep();
    }
}

//1、创建接口
interface Inner1{
    //定义接口中的抽象方法
    void save();
    void get();
}
//2、创建抽象类
abstract class Inner2{
    public void play(){
        System.out.println("我是抽象类的普通方法");
    }
    abstract public void drink();//抽象方法没有方法体
}
//3、创建普通类
class Inner3{
    public void eat(){
        System.out.println("干饭人，干饭时间");
    }
    public void sleep(){
        System.out.println("早睡早起身体好");
    }
}