package InnerClassExer;

public class InnerClassExer2 {
    public static void main(String[] args) {
        new Outer2().getInner2Show();
    }
}
class Outer2{

    public void getInner2Show(){//提供外部类公共的方法,在方法内部创建Inner2内部类对象,调用内部类方法

        Inner2 inner2=new Inner2();//创建内部类对象
        inner2.show();//调用方法
    }

    private class Inner2{//创建私有内部类
        public void show(){
            System.out.println("内部类，加油，还得坚持学习");
        }
    }
}