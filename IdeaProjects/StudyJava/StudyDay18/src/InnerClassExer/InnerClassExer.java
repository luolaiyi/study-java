package InnerClassExer;

public class InnerClassExer {
    public static void main(String[] args) {
        Outer.Inner inner = new Outer().new Inner();//创建内部类对象
        inner.show();//调用内部类方法
        System.out.println(inner.value);//调用内部类成员变量
        new Outer().eat();

    }
}
class Outer{
    String name;
    private int age;

    public void eat(){
        System.out.println("外部类，啥也别说了，我要干饭去了");
        Inner inner=new Inner();//外部类想要使用内部类资源，需要在方法中创建内部类对象，然后通过内部类对象来调用
        //内部类的资源
        System.out.println(inner.value);//调用内部类成员变量
        inner.show();//调用成员方法
    }

    class Inner{
        int value=22;

        public void show(){
            System.out.println("内部类，请开始你的表演");
            System.out.println(name);//直接使用外部类的成员变量
            System.out.println(age);//直接使用外部类的成员变量
            //eat();//直接使用外部类的成员方法
        }

    }
}