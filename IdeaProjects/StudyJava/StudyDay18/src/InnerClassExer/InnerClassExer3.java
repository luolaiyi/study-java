package InnerClassExer;

public class InnerClassExer3 {
    public static void main(String[] args) {
    Outer3.Inner3 inner3=new Outer3.Inner3();//new Outer3.Inner3(),内部类是静态的，可以通过直接点类名来创建
    inner3.sleep();
    new Outer3.Inner3().sleep();//匿名的内部类对象调用sleep()
    Outer3.Inner3.study();//访问静态内部类中的静态资源--链式加载

    }
}
class Outer3{
    static class Inner3{//静态内部类
        public void sleep(){
            System.out.println("静态内部类，虽然很困，但是还是要坚持");
        }

        public static void study(){//静态方法
            System.out.println("静态内部类的静态方法，我要努力学习挣更多的钱");
        }
    }
}