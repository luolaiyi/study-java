package InnerClassExer;

public class InnerClassExer4 {
    public static void main(String[] args) {
        //当在外部类show()中创建局部内部类对象并且进行功能调用后,内部类的功能才能被调用
        new Outer4().show();
    }
}
class Outer4{
    public void show(){
        class Inner4{
            String name="罗来意";
            int age=25;

            public void study(){
                System.out.println("局部内部类，我现在已经不困了，写代码越来越兴奋了，起飞");
            }

        }
        Inner4 inner4 = new Inner4();
        System.out.println(inner4.name);
        System.out.println(inner4.age);
        inner4.study();
    }
}