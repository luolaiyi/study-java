package InnerClassExer;

public class InnerClassExer5 {
    public static void main(String[] args) {
      new Inner1() {
          @Override
          public void study() {
              System.out.println("重写接口方法");
          }

          @Override
          public void sleep() {
              System.out.println("重写接口方法");

          }
      }.sleep();
      new Inner2() {
          @Override
          public void show() {
              System.out.println("重写抽象类方法");
          }
      }.eat();
      new Inner3().drink();
      Inner3 inner3=new Inner3();
      inner3.play();
    }
}
interface Inner1{
    void study();
    void sleep();

}

abstract class Inner2{
  public void eat(){
      System.out.println("抽象类,我想干饭了，敲代码敲的我都饿了");
  }

  abstract public void show();
}

class Inner3{
    public void play(){
        System.out.println("已经过了玩耍的年纪了");
    }
    public void drink(){
        System.out.println("我想喝可乐，我就是不说，就是玩儿");
    }
}