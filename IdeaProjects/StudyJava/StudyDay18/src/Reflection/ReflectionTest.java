package Reflection;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*
暴力反射
 */
public class ReflectionTest {
    //暴力反射，获取和设置私有属性
    @Test
    public void getField() throws Exception {
        //获取字节码对象
        Class<?> person1 = Person.class;
        Field field = person1.getDeclaredField("name");//2.获取私有属性
        //获取属性的类型
        System.out.println(field.getType().getName());
        System.out.println(field.getType());

        //设置属性的值
        //set(m,n)--m:要给哪个对象设置值，n:是要设置的具体值
        //通过反射的方式创建对象
        Object obj = person1.newInstance();//多态//4.1没有对象就通过反射的方式创建对象
        //给指定对象设置值
        field.setAccessible(true);//4.2暴力反射!!!设置私有可见
        field.set(obj,"沙雕");//4.3给指定对象设置值
        //获取私有属性的值
        System.out.println(field.get(obj));

    }

    @Test
    public void getFunction() throws Exception {
        Class<?> person2 = Person.class;//获取字节码对象
        Method method = person2.getDeclaredMethod("save",String.class,int.class);
        Object obj = person2.newInstance();
        method.setAccessible(true);
        method.invoke(obj,"沙雕",22);



    }


}
