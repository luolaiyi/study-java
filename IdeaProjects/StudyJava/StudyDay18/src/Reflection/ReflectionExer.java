package Reflection;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*
暴力反射作业练习
 */
public class ReflectionExer {
    @Test
    public void getField() throws Exception {
        Class<?> per=Person.class;//创建字节码对象
        Field field = per.getDeclaredField("age");//获取私有属性age
        System.out.println(field.getName());
        System.out.println(field.getType().getName());

        Object object = per.newInstance();//通过反射反射创建对象
        field.setAccessible(true);//暴力反射，设置私有属性可见
        field.set(object,18);//给指定对象的属性设置值
        System.out.println(field.get(object));

    }

    @Test
    public void getFunction() throws Exception {
        Class<?> person = Person.class;
        Method method = person.getDeclaredMethod("update", int.class, String.class);

        Object object= person.newInstance();
        method.setAccessible(true);
        method.invoke(object,22,"泡泡老师");


    }
}
