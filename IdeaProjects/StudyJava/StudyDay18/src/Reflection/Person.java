package Reflection;
/*
测试暴力反射物料类
 */
public class Person {
    //提供私有属性
    private String name;
    private int age;

    //提供私有方法
    private void save(String s,int i){
        System.out.println("正在保存中...");
    }
    private void update(int a,String s){
        System.out.println("正在更新中..."+s+a);
    }
}
