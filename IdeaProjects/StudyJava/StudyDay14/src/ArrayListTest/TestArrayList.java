package ArrayListTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;

public class TestArrayList {
    public static void main(String[] args) {
        //1.创建对象,使用的是无参构造
        //底层会自动帮我们创建数组存放对象,并且数据的初始容量是10
        ArrayList<Integer> list=new ArrayList<Integer>();
        //添加集合元素
        list.add(100);
        list.add(200);
        list.add(300);
        list.add(400);
        list.add(500);

        System.out.println(list);
        System.out.println(list.contains(100));
        System.out.println(list.get(1));
        System.out.println(list.indexOf(500));
        list.set(2,200);
        System.out.println(list);
        System.out.println(list.lastIndexOf(200));
        System.out.println(list.isEmpty());
        System.out.println(list.remove(1));
        /**
         *Exception in thread "main" java.lang.IndexOutOfBoundsException: Index: 300, Size: 6
         *这是根据下标来删除元素的,而此集合没有下标300,最大下标为6,所以会数组下标越界
         *System.out.println(list.remove(300));--错误
         *如果想根据具体的元素内容移除元素,需要先把int类型的数据转成Integer数据类型
         * * */
        System.out.println(list.remove(Integer.valueOf(200)));
        //System.out.println(list.remove(200));
        //Exception in thread "main" java.lang.IndexOutOfBoundsException: Index: 200, Size: 3
        System.out.println(list);
        System.out.println(list.size());
        System.out.println(Arrays.toString(list.toArray()));

        //迭代方式
        //For循环
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("方式一");
        //高效for循环
        for(Integer integer:list){
            System.out.println(integer);
        }
        System.out.println("方式二");
        //迭代器
        Iterator<Integer> it=list.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        System.out.println("方式三");
        //迭代器二
        ListIterator<Integer> iterator=list.listIterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("方式四");


    }
}
