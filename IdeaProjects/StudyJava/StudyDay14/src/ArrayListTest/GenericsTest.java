package ArrayListTest;

import java.util.ArrayList;
import java.util.List;

/*
泛型测试
 */
public class GenericsTest {
    public static void main(String[] args) {
        String [] str= new String [5];
        //String [] s={"22"};
        str[0]="龙傲天";
        str[2]="沙雕";
        //System.out.println(Arrays.toString(str));
        //str[1]=1;编译报错
        //创建集合
        List list1 = new ArrayList();
        list1.add("龙傲天");
        list1.add(10086);
        list1.add(8.8);
        list1.add('a');
        System.out.println(list1);

        List<String> list2 = new ArrayList<String>();
        list2.add("张巧丽");//约束了类型后只能传String类型
        System.out.println(list2);
        //list2.add(5);编译报错
        //<>必须是引用类型，不能是基本类型
        List<Integer> list3= new ArrayList<Integer>();
        list3.add(100);
        list3.add(200);
        System.out.println(list3);



    }
}
