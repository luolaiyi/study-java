package ArrayListTest;

import java.util.ArrayList;
import java.util.Iterator;

/*
四种迭代方式练习
ArrayList存在java.util包中
内部是用数组结构存放数据,封装数组的操作,每个对象都有下标
内部数组默认的初始容量是10,如果不够会以1.5倍的容量增长
查询快,增删数据效率会低
 */
public class DemoArrayList {
    public static void main(String[] args) {
        ArrayList<String> list=new ArrayList<String>();
        list.add("我");
        list.add("要");
        list.add("成");
        list.add("为");
        list.add("Java");
        list.add("大");
        list.add("神");
        System.out.println(list);
        //迭代方式
        //1、for循环
        System.out.println("方式一：for循环");
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        //2、增强for循环
        System.out.println("方式二：增强for循环");
        for(String str:list){
            System.out.println(str);
        }
        //3、Iterator迭代器
        System.out.println("方式三：Iterator迭代器");
        Iterator<String> it=list.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        //4、ListIterator迭代器
        System.out.println("方式四：ListIterator迭代器");
        Iterator<String> itt=list.listIterator();
        while (itt.hasNext()){
            System.out.println(itt.next());
        }

    }
}
