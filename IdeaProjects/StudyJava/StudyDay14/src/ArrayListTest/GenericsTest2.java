package ArrayListTest;
/*
测试泛型的优点
 */
public class GenericsTest2 {
    public static void main(String[] args) {
        Integer[] in = {1, 2, 3, 4};
        print(in);
        String[] str = {"我", "爱", "学", "习"};
        print(str);
        Double[] dou = {6.6, 6.66, 6.666, 6.6666};
        print(dou);

    }
    /**1.泛型可以实现通用代码的编写,使用E表示元素的类型是Element类型 -- 可以理解成神似多态*/
    /**2.泛型的语法要求:如果在方法上使用泛型,必须两处同时出现,一个是传入参数的类型,一个是返回值前的泛型类型,表示这是一个泛型*/
    private static <E> void print(E[] e){
        for (E i:e){
            System.out.println(i);
        }
    }
//    private static void print(Integer[] x){
//        for (int i=0;i<x.length;i++){
//            System.out.println(x[i]);
//        }
//    }
//    private static void print(String[] y){
//        /**
//         //		 * 高效for/foreach循环--如果只是单纯的从头到尾的遍历,使用增强for循环
//         //		 * 好处:比普通的for循环写法简便,而且效率高
//         //		 * 坏处:没有办法按照下标来操作值,只能从头到尾依次遍历
//         //		 * 语法:for(1 2 : 3){代码块} 3是要遍历的数据  1是遍历后得到的数据的类型 2是遍历起的数据名
//         //		 */
//        for (String s:y){
//            System.out.println(s);
//        }
//
//    }
//    private static void print(Double[] z){
//        for (double d:z){
//            System.out.println(d);
//        }
//    }
}