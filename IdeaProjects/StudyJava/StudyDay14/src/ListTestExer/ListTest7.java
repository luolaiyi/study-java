package ListTestExer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/*
 * 创建一个List集合，并存放10个随机数，然后排序该集合
 * 后输出
 * @author Xiloer
 */
public class ListTest7 {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<Integer>();
        Random random = new Random();
        for (int i=1;i<=10;i++){
            list.add(random.nextInt(100));
        }
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);

    }
}
