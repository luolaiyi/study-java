package ListTestExer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * 创建一个List集合，并添加元素0-9
 * 将集合转换为一个Integer数组并输出数组每一个元素
 * @author Xiloer
 */
public class ListTest5 {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<Integer>();
        for (int i=0;i<10;i++){
            list.add(i);
        }
        System.out.println(list);

        //Integer[] array=list.toArray(new Integer[list.size()]);//
        Integer[] array=new Integer[list.size()];
        Integer[] listarray=list.toArray(array);
        System.out.println(Arrays.toString(array));


    }
}
