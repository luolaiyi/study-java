package ListTestExer;

import java.util.ArrayList;
import java.util.List;

/*
 * 创建一个List集合并添加元素0-9
 * 然后获取子集[3,4,5,6]
 * 然后将子集元素扩大10倍
 * 然后输出原集合。
 * 之后删除集合中的[7,8,9]
 * @author Xiloer
 */
public class ListTest4 {
    public static void main(String[] args) {
        ArrayList<Integer> list=new ArrayList<Integer>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        System.out.println(list);
        List<Integer> sublist=list.subList(3,7);//如果对截取的子串进行操作，需要重新创建一个新的集合存放数据
        System.out.println(sublist);

        for (int i=0;i<sublist.size();i++){
            sublist.set(i,sublist.get(i)*10);//sublist.get(i)*10,遍历集合数据*10，再对各位置元素进行重置
        }
        System.out.println(sublist);//[30, 40, 50, 60]
        System.out.println(list);//[0, 1, 2, 30, 40, 50, 60, 7, 8, 9],因为截取的子段从list而来，subject
        //改变，list跟着改变;

        list.subList(7,10).clear();
        System.out.println(list);


    }
}
