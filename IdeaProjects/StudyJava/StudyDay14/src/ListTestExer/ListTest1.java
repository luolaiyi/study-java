package ListTestExer;
/*
/**
 * 创建一个集合c1，存放元素"one","two","three"
 * 再创建一个集合c2，存放元素"four","five","six"
 * 然后将c2元素全部存入c1集合
 * 然后在创建集合c3,存放元素"one,five"
 * 然后输出集合c1是否包含集合c3的所有元素
 * 然后将c1集合中的"two"删除后再输出c1集合
 * @author Xiloer
 *
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ListTest1 {
    public static void main(String[] args) {
        ArrayList<String> list1=new ArrayList<String>();
        ArrayList<String> list2=new ArrayList<String>();
        ArrayList<String> list3=new ArrayList<String>();
        Set<String> set=new HashSet<>();
        list1.add("one");
        list1.add("two");
        list1.add("three");
        System.out.println(list1);

        list2.add("four");
        list2.add("five");
        list2.add("six");
        System.out.println(list2);

        list3.add("one");
        list3.add("five");
        System.out.println(list3);

        System.out.println(list1.addAll(list2));
        System.out.println(list1);
        System.out.println(list1.containsAll(list3));
        System.out.println(list1.remove("two"));
        System.out.println(list1);


    }
}
