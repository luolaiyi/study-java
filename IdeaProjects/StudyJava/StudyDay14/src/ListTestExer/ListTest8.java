package ListTestExer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 * 通过控制台输入3个日期(yyyy-MM-dd格式)，然后转换为Date对象后存入
 * 集合，然后对该集合排序后输出所有日期。
 * @author Xiloer
 */
public class ListTest8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<Date> dateList=new ArrayList<Date>();
        for (int i=1;i<=3;i++){
            System.out.println("请输入第"+i+"个日期：");
            String str = sc.nextLine();
            try {
                Date date = simpleDateFormat.parse(str);
                dateList.add(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        System.out.println(dateList);
        Collections.sort(dateList);
        System.out.println(dateList);
    }

}
