package Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
本类用于测试List接口
 */
public class ListTest {
    public static void main(String[] args) {
        List<String> list= new ArrayList<String>();//创建List接口多态对象，接口不能实例化
        //向集合中添加元素
        list.add("大力娃");
        list.add("千顺娃");
        list.add("头铁娃");
        list.add("喷火娃");
        list.add("喷水娃");
        list.add("隐身娃");
        list.add("小紫娃");
        System.out.println(list);
        //list.clear();清空集合
        //System.out.println(list);
        System.out.println(list.remove("喷水娃"));//移出集合中的指定元素
        System.out.println(list.size());
        System.out.println(list.hashCode());//获取哈希码
        System.out.println(list.isEmpty());//判断集合是否为空
        System.out.println(list.contains("喷水娃"));//判断集合是否包含喷水娃
        System.out.println(list.equals("喷水娃"));//判断集合对象list是否与喷水娃相等
        System.out.println(Arrays.toString(list.toArray()));

        //测试List接口自身方法
        list.add("小蝴蝶");
        System.out.println(list);
        list.add(0,"蛇精");//在指定的索引处添加元素，首个位置下标为0
        System.out.println(list);
        System.out.println(list.get(1));
        System.out.println(list.indexOf("小蝴蝶"));//判断指定元素的索引
        list.add("小蝴蝶");
        System.out.println(list.lastIndexOf("小蝴蝶"));//判断指定元素最后一次出现的索引位置
        System.out.println(list.remove(5));//移出指定索引处的集合元素
        System.out.println(list);
        System.out.println(list.set(0,"仙女姐姐"));//重置指定索引处的集合元素
        System.out.println(list);
        List<String> sublist= list.subList(1,5);//截取索引1到4的集合元素
        System.out.println(sublist);

        //测试集合间的操作
        List<String> list2=new ArrayList<String>();
        //添加集合元素
        list2.add("111");
        list2.add("222");
        list2.add("333");
        list2.add("444");
        System.out.println(list.addAll(list2));//把list2中全部元素添加到list集合中
        System.out.println(list);
        System.out.println(list.addAll(1,list2));//在list集合的指定索引位置添加list2中的全部元素
        System.out.println(list);
        System.out.println(list2);
        System.out.println(list.containsAll(list2));//判断list集合是否包含list2
        System.out.println(list);
        System.out.println(list.retainAll(list2));//获取list集合中与list2集合中相同的集合元素
        System.out.println(list);
    }
}
