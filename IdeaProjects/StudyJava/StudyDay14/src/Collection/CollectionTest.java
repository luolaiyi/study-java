package Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/*
本类用来测试Collection接口
 */
public class CollectionTest {
    public static void main(String[] args) {
        //Collection c = new Collection() ;报错，接口不能实例化
        //<Integer>是泛型，用来约束集合的元素类型，只能写引用类型，不能写基本类型
        Collection<Integer> c =new ArrayList<Integer>();//多态
        //1、向集合中添加元素
        c.add(100);
        c.add(200);
        c.add(300);
        c.add(400);
        System.out.println(c);
        //2、清空集合中的元素
        //c.clear();
        //System.out.println(c);
        System.out.println(c.contains(300));//true,判断集合中是否包含指定元素300
        System.out.println(c.hashCode());//4104521,打印哈希值
        System.out.println(c.isEmpty());//false,判断集合是否为空
        System.out.println(c.remove(100));//移出指定元素100
        System.out.println(c);
        System.out.println(c.size());//获取集合元素个数
        System.out.println(c.equals(200));//flase,判断集合对象与元素200是否相等

        System.out.println(c.toArray());//[Ljava.lang.Object;@1540e19d,将集合转为数组
        System.out.println(Arrays.toString(c.toArray()));//[200, 300, 400]
//        Object[] obj=c.toArray();
//        System.out.println(Arrays.toString(obj));

        //测试集合间的操作
        Collection<Integer> cc= new ArrayList<Integer>();
        //向集合中添加元素
        cc.add(1);
        cc.add(2);
        cc.add(3);
        cc.add(4);
        System.out.println(cc);//[1, 2, 3, 4]
        c.addAll(cc);//把cc集合中的全部元素添加到c集合中
        System.out.println(c);//[200, 300, 400, 1, 2, 3, 4]
        System.out.println(c.contains(cc));//false,查看c集合中是否包含一个叫cc的元素
        System.out.println(c.containsAll(cc));//true

        System.out.println(c);//[200, 300, 400, 1, 2, 3, 4]
        System.out.println(cc);//[1, 2, 3, 4]
        //System.out.println(c.retainAll(cc));//true,取c 集合中与cc集合中相同的元素，取交集
        //System.out.println(c);//[1, 2, 3, 4]
        //System.out.println(cc);//[1, 2, 3, 4]


        //System.out.println(c.removeAll(cc));//true
        //System.out.println(c);//[200, 300, 400]

        //用来遍历\迭代集合中的元素
        //1、获取集合的迭代器c.iterator()
        //2、判断集合是否有下个元素 it.hasNext()
        //3、获取当前迭代到的元素 it.next()
        Iterator<Integer> it = cc.iterator();
        while (it.hasNext()){
            Integer num=it.next();
            System.out.println(num);
        }



    }
}
