package Collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/*

 */
public class ListTest2 {
    public static void main(String[] args) {
        //创建List接口对象
        List<String> list= new ArrayList<String>();
        //向集合中添加元素
        list.add("喜洋洋");
        list.add("美洋洋");
        list.add("沸洋洋");
        list.add("懒洋洋");
        list.add("老村长");
        /* 集合迭代的方式:* 1.for循环* 2.增强for循环* 3.iterator* 4.listIterator* */
        //1.for循环,List集合是有序的，元素有下标的
        for (int i=0;i<list.size();i++){
            String str= list.get(i);
            System.out.println(str);
        }
        System.out.println("*************************************************");
        //2.增强for循环
        for (String str:list){
            System.out.println(str);
        }
        System.out.println("*************************************************");
        //3.iterator*
        //1、获取集合的迭代器c.iterator()
        //2、判断集合是否有下个元素 it.hasNext()
        //3、获取当前迭代到的元素 it.next()
        Iterator<String> it=list.iterator();
        while (it.hasNext()){
            String str=it.next();
            System.out.println(str);
        }
        //3:listIterator()是List接口特有的
        //Iterator<E> Iterator --父接口 --hasNext() --next() --remove()
        //ListIterator<E> ListIterator --子接口,拥有父接口的方法,也有自己特有的方法(逆向迭代)
        //public interface ListIterator<E> extends Iterator<E>
        System.out.println("*************************************************");
        ListIterator<String> it2=list.listIterator();
        while (it2.hasNext()){
            System.out.println(it2.next());
        }

    }
}
