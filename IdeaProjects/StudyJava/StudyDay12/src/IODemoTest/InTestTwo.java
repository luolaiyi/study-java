package IODemoTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

public class InTestTwo {
    public static void main(String[] args) {
     //method1();
        method2();
    }

    private static void method1() {
        try {
            Reader reader1 = new FileReader("D:\\ready\\1.txt");
            int num;
            while ((num=reader1.read())!=-1){
                System.out.println(num);
            }
            reader1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void method2(){
        try {
            Reader reader2= new BufferedReader(new FileReader("D:\\ready\\1.txt"));
            int num;
            while ((num=reader2.read())!=-1){
                System.out.println(num);
            }
            reader2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
