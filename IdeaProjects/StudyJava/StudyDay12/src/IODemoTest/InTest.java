package IODemoTest;

import java.io.*;

/*
本类用于测试文件的读取
 */
public class InTest {
    public static void main(String[] args) {
        //method1();//读取字节流
        method2();

    }
    private static void method1(){
        try {
            InputStream in=new FileInputStream(new File("D:\\ready\\1.txt"));
            //开始读取read();
            //System.out.println(in.read());//-1
            int num;
            while ((num=in.read())!=-1){
                System.out.println(num);
            }
            in.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void method2(){
        try {
            InputStream inn= new BufferedInputStream(
                    new FileInputStream("D:\\ready\\1.txt"));

            int num;
            while ((num=inn.read())!=-1){
                System.out.println(num);
            }
            inn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
