package IODemoTest;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/*
本类用于测试文件类
 */
public class FileTest {
    public static void main(String[] args) throws IOException {
        //构造函数的参数是string类型的pathname，可以是文件路径，也可以是文件夹路径
        //\在代码中有特殊的意义，所以想要真正表示这是一个\需要用\进行转义
        //注意：需要自己手动添加内容到对应的txt文件夹内
        File file = new File("D:\\ready\\1.txt");

        //测试文件常用方法
        System.out.println(file.length());//11,获取指定文件的字节量大小
        System.out.println(file.exists());//true,判断指定文件是否存在
        System.out.println(file.isFile());//true,判断指定内容是否为文件
        System.out.println(file.isDirectory());//flase,判断指定内容是否为文件夹
        System.out.println(file.getName());//1.txt,获取文件名称
        System.out.println(file.getParent());//D:\ready,获取文件夹上级文件目录
        System.out.println(file.getAbsoluteFile());//D:\ready\1.txt
        System.out.println(file.getAbsolutePath());//D:\ready\1.txt，获取指定内容的绝对路径
        //System.out.println(Arrays.toString(new long[]{file.lastModified()}));

        //创建于删除
        file = new File("D:\\ready\\2.txt");//创建文件对象
        //如果指定创建文件的文件夹不存在,会报错:java.io.IOException: 系统找不到指定的路径。
        //file = new File("D:\\rrrr\\2.txt");
        //可能会发生错误,所以需要抛出异常IOException*/
        System.out.println(file.createNewFile());//true，调用方法创建文件
        file = new File("D:\\ready\\m");
        System.out.println(file.mkdir());//true,创建不存在的单级文件夹,m文件夹自动创建成功
        file = new File("D:\\ready\\a\\b\\c");
        System.out.println(file.mkdirs());//true,创建不存在的多层文件夹

        System.out.println(file.delete());
        file = new File("D:\\ready\\a");
        System.out.println(file.delete());//flase,因为a目录里还有b目录
        file = new File("D:\\ready\\2.txt");
        System.out.println(file.delete());

        //文件列表测试
        file = new File("D:\\ready");
        String[] listName=file.list();
        System.out.println(Arrays.toString(listName));

        File[] fs=file.listFiles();
        System.out.println(Arrays.toString(fs));
        System.out.println(fs[0].length());


    }
}
