package NumberDemoTest;

import java.util.Scanner;

/*
正则表达式应用，测试输入的身份证号，测试用户输入的是否正确
 */
public class TestRegex {
    public static void main(String[] args) {
        System.out.println("请输入你的身份证号：");
        Scanner scanner=new Scanner(System.in);
        String input=scanner.nextLine();//获取输入的身份证号

        //编辑正则表达式
        //身份证号的规律：一般18位纯数字或数字加字母；前17位都是数字，最后一位可能为X
        String regex="[0-9]{17}[0-9X]";
        //String value="\\d{17}[0-9X]";

        //判断用户输入的数据是否符合正则表达式的规则
        if (input.matches(regex)){//matches()是String类提供的功能,可以用来判断字符串是否符合正则表达式的要求
            System.out.println("身份证号输入正确");
        }else {
            System.out.println("身份证号输入错误");
        }

    }


}
