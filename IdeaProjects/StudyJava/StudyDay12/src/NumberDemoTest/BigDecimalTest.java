package NumberDemoTest;

import java.math.BigDecimal;
import java.util.Scanner;

public class BigDecimalTest {
    public static void main(String[] args) {
        f2();

    }

    private static void f2(){
        System.out.println("请输入您要计算的两个小数：");
        double i=new Scanner(System.in).nextDouble();
        double j=new Scanner(System.in).nextDouble();

        //2.创建工具类对象,把基本类型的a和b交给工具类对象BigDecimal来保存
        /**1.最好不要使用double作为构造函数的参数,不然还会产生不精确的现象,有坑!!!!*/
        /**2.最好使用重载的,参数类型是String的构造函数,double转String,直接拼个空串就可以*/
        BigDecimal ii=new BigDecimal(i+"");
        BigDecimal jj=new BigDecimal(j+"");

        BigDecimal total;

        total=ii.add(jj);//加法
        System.out.println(total);

        total=ii.subtract(jj);//减法
        System.out.println(total);

        total=ii.multiply(jj);//乘法
        System.out.println(total);
        //divide(m,n,o)m表示被除数；n表示保留的位数；o表示保留方式，一般用四舍五入方式
        total=ii.divide(jj,3, BigDecimal.ROUND_HALF_UP);//除法，除法运算除不尽时会抛出异常
        System.out.println(total);


    }

    private  static  void f1(){
        System.out.println("请输入您要计算的两个小数：");
        double i=new Scanner(System.in).nextDouble();
        double j=new Scanner(System.in).nextDouble();

        System.out.println(i+j);
        System.out.println(i-j);
        System.out.println(i*j);
        System.out.println(i/j);
    }
}
