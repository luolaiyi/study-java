package NumberDemoTest;
/*
测试自动装箱与自动拆箱
 */
public class TestBox {
    public static void main(String[] args) {
        Integer integer1=new Integer(127);
        Integer integer2=Integer.valueOf(127);

        Integer integer3=5;//自动装箱，将基本类型包装成Integer类型：Integer.valueOf(127)
        int i=integer1;//自动拆箱，将Intefer类型转换成为基本类型：int i = a.intValue();intValue拆箱子
        System.out.println(i);
    }
}
