package NumberDemoTest;
/*
本类用于测试包装类
 */
public class NumberTest {
    public static void main(String[] args) {
        //创建Ingeter的方式,Ingeter包装类型的默认值null
        Integer integer1=new Integer(5);//5
        System.out.println(integer1);
        //Ingeter有一个高效效果(-128-127),在此范围内，相同的数据只会保存一次，后续再保存都是使用之前保存过的数据
        Integer integer2=Integer.valueOf(127);
        Integer integer3=Integer.valueOf(127);
        System.out.println(integer2==integer3);//true
        //超出高效范围
        Integer integer4=Integer.valueOf(1270);
        Integer integer5=Integer.valueOf(1270);
        System.out.println(integer4==integer5);//flase

        //创建double类型对应包装类型Double,Double不具有高效性
        Double double1=new Double(3.14);
        Double double2=Double.valueOf(3.14);
        Double double3=Double.valueOf(3.14);
        System.out.println(double1==double2);//flase
        System.out.println(double2==double3);//flase

        //测试常用的方法,转换类型
        System.out.println(integer1.parseInt("800")+10);//810
        System.out.println(double1.parseDouble("6.66")+2.22);//8.88
    }
}
