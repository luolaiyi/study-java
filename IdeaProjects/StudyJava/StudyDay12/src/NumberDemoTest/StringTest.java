package NumberDemoTest;

import java.util.Scanner;

/*
 * 要求用户输入一个字符串，然后若该字符串是一个整数，则转换为整数后输出乘以10后的结果
 * 若是小数，则转换为一个小数后输出乘以5后的结果，若不是数字则输出"不是数字"
 */
public class StringTest {
    public static void main(String[] args) {
        //整数的正则表达式
        String intReg = "\\d+";
        //小数的正则表达式
        String douReg = "\\d+\\.\\d+";


        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个数字：");
        String str = scanner.nextLine();

        if(str.matches(intReg)){
            int num = Integer.parseInt(str);
            System.out.println("整数为："+num+"  num * 10 = "+(num*10));
        }else if(str.matches(douReg)){
            double num= Double.parseDouble(str);
            System.out.println("小数为:"+num+"   num * 5 = "+(num*5));
        }else {
            System.out.println("不是数字");
        }

    }
}
