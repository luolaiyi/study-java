package LinkedList;

import java.util.LinkedList;

/*
本类用于LinkedList的相关测试
 */
public class LinkedListTest {
    public static void main(String[] args) {
        //创建对象
        LinkedList<String> list = new LinkedList<>();
        LinkedList<String> list2 = new LinkedList<>();
        //添加集合元素
        list.add("孙悟空");
        list.add("唐僧");
        list.add("猪八戒");
        list.add("沙师弟");
        list.add("白龙马");
        //测试collection方法
        System.out.println(list);
        list.add(0,"龙傲天");//在指定位置添加集合元素
        System.out.println(list);
        System.out.println(list.contains("白龙马"));//判断集合中是否包含指定集合元素
        System.out.println(list.get(0));//获取集合中索引位置为0的集合元素
        System.out.println(list.indexOf("沙师弟"));//获取指定集合元素在集合中的索引位置
        System.out.println(list.remove("猪八戒"));//移出指定元素
        System.out.println(list);

        list.addFirst("女儿国国王");//添加首元素
        list.addLast("如来");//添加尾元素
        System.out.println(list);

        System.out.println(list.getFirst());//获取首元素
        System.out.println(list.getLast());//获取尾元素

        System.out.println(list.removeFirst());//移出首元素
        System.out.println(list.removeLast());//移出尾元素
        System.out.println(list);

        list2.add("完美世界");
        list2.add("最强狂兵");
        list2.add("神级奶爸");
        list2.add("斗罗大陆");
        list2.add("万界神主");
        System.out.println(list2);
        System.out.println(list2.element());//获取但不移出链表中的首元素
        System.out.println(list2.peek());//获取但不移出链表中的首元素
        System.out.println(list2.peekFirst());//获取但不移出链表中的首元素
        System.out.println(list2.peekLast());//获取但不移出链表中的尾元素
        System.out.println(list2.offer("坏蛋是怎么炼成的"));//把指定元素添加到链表的末尾
        System.out.println(list2);
        System.out.println(list2.offerFirst("斗破苍穹"));//把指定元素添加到链表的首部
        System.out.println(list2);
        System.out.println(list2.offerLast("剑来"));//把指定元素添加到链表的末尾
        System.out.println(list2);

        System.out.println(list2.poll());//获取本次链表中被删除的首元素
        System.out.println(list2);
        System.out.println(list2.pollFirst());//获取本次链表中被删除的首元素
        System.out.println(list2);
        System.out.println(list2.pollLast());//获取本次链表中被删除的尾部元素
        System.out.println(list2);

    }
}
