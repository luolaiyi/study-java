package Thread;
//这个类用来测试多线程售票
//需求:设计4个售票窗口，总计售票100张
public class SaleTickets1 {
    public static void main(String[] args) {
        TicketThread t1 = new TicketThread();
        TicketThread t2 = new TicketThread();
        TicketThread t3 = new TicketThread();
        TicketThread t4 = new TicketThread();

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }
}
class TicketThread extends Thread {
    public static int ticket = 100;
    Object object = new Object();

    @Override
    public void run() {
        while (true) {
            //synchronized (object){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
            System.out.println(getName() + " = " + ticket--);
            if (ticket <= 0) {
                break;
            }
        }
    }
 }
//}