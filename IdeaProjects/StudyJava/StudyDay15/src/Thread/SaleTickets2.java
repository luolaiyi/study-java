package Thread;

public class SaleTickets2 {
    public static void main(String[] args) {
        TicketRunnable target =new TicketRunnable();

        Thread thread1=new Thread(target);
        Thread thread2=new Thread(target);
        Thread thread3=new Thread(target);
        Thread thread4=new Thread(target);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();


    }
}
class TicketRunnable implements Runnable{
    static int ticket=100;
    Object object=new Object();

    @Override
    public void run() {
        while (true) {
            synchronized (object) {
                if (ticket > 0) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + " = " + ticket--);
                }
                if (ticket <= 0) {
                    break;
                }
            }
        }
    }
}