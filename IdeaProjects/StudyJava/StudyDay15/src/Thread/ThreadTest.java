package Thread;

/*
本类用于实现多线程编程方式一extends Thread
继承Thread类
优点: 编写简单,如果需要访问当前线程,无需使用Thread.currentThread()方法,直接使用this即可获得当前线程
缺点: 自定义的线程类已继承了Thread类,所以后续无法再继承其他的类
 */
public class ThreadTest {
    public static void main(String[] args) {
      MyThread myThread1=new MyThread();
      myThread1.start();

      MyThread myThread2=new MyThread();
      myThread2.start();

      MyThread myThread3=new MyThread("沙雕");
      myThread3.start();


    }
}
//自定义一个类Thread
class MyThread extends Thread{
    //线程中的业务必须写在run方法()里


    public MyThread() {
    }

    public MyThread(String name) {
        super(name);
    }

    @Override
    public void run() {

        //super.run();
        for (int i=1;i<=10;i++){
            /**getName()可以获取正在执行任务的线程名称,是从父类中继承的方法,可以直接使用*/
            System.out.println(i+"="+this.getName());
        }
    }
}