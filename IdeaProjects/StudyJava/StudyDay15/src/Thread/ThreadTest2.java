package Thread;
/**
 * 本类用于测试多线程编程方式2 implements Runnable
 * 实现Runnable接口
 * 优点: 自定义的线程类只是实现了Runnable接口或Callable接口,后续还可以继承其他类,在这种方式下,
 * 多个线程可以共享同一个target对象,所以非常适合多个相同线程来处理同一份资源的情况,
 * 从而可以将CPU、代码、还有数据分开(解耦),形成清晰的模型,较好地体现了面向对象的思想
 * 缺点: 编程稍微复杂,如想访问当前线程,则需使用Thread.currentThread()方法
 * */
public class ThreadTest2 {
    public static void main(String[] args) {
        MyRunnable target =new MyRunnable();

        Thread thread=new Thread(target);
        thread.start();

        Thread thread2=new Thread(target);
        thread2.start();

        Thread thread3=new Thread(target,"沙雕");
        thread3.start();



    }
}
class MyRunnable implements Runnable{
    @Override
    public void run() {
        for (int i=1;i<=10;i++){
            //问题:Runnable接口中,没有提供多余的方法维度只有一个run()
            //Thread.currentThread()获取当前正在执行业务的线程对象 getName()获取此线程对象的名称
            System.out.println(i+"="+Thread.currentThread().getName());

        }
    }
}