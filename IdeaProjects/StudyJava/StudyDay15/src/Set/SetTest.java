package Set;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
本类用于Set集合测试
 */
public class SetTest {
    public static void main(String[] args) {
        Set<String> set=new HashSet<>();//创建集合，多态
        set.add("牛气冲天");//向set集合添加数据
        set.add("牛气冲天");//向set集合添加重复的数据
        set.add("牛气冲天");//向set集合添加重复的数据
        set.add("虎虎生威");//向set集合添加数据
        set.add(null);
        System.out.println(set);

        System.out.println(set.contains("沙雕"));
        System.out.println(set.equals("牛气冲天"));
        System.out.println(set.hashCode());//1957659410
        System.out.println(set.remove(null));
        System.out.println(set);
        System.out.println(set.size());
        System.out.println(Arrays.toString(set.toArray()));

        Set<String> set2 = new HashSet();
        set2.add("小老鼠");//给set2集合添加指定元素
        set2.add("小牛犊");//给set2集合添加指定元素
        set2.add("小脑斧");//给set2集合添加指定元素
        set2.add("小兔纸");//给set2集合添加指定元素
        System.out.println(set2);//查看set2集合中的元素
        System.out.println(set.addAll(set2));
        System.out.println(set);
        System.out.println(set.containsAll(set2));
        //System.out.println(set.removeAll(set2));
        System.out.println(set.retainAll(set2));
        System.out.println(set);

        //创建迭代器，遍历集合元素，打印输出
        Iterator<String> iterator=set2.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
}
