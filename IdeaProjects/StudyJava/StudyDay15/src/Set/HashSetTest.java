package Set;

import java.util.HashSet;
import java.util.Iterator;

/*
本类用于测试hashset
 */
public class HashSetTest {
    public static void main(String[] args) {
        //创建hashSet对象
        HashSet<Integer> hashSet=new HashSet<>();
        //添加元素
        hashSet.add(100);
        hashSet.add(200);
        hashSet.add(200);
        hashSet.add(300);
        hashSet.add(700);
        System.out.println(hashSet);
        System.out.println(hashSet.contains(200));

        Iterator<Integer> iterator=hashSet.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
