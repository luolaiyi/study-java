package Map;

import java.util.*;

/*
本类用于测试Map接口
Map可以根据键来提取对应的值
Map的键不允许重复,如果重复,对应的值会被覆盖
Map存放的都是无序的数据
Map的初始容量是16,默认的加载因子是0.75
 */
public class MapTest {
    public static void main(String[] args) {
        //创建Map对象，Map中的数据要符合映射规则，一定要注意同时指定K和V的数据类型
        Map<Integer,String> map=new HashMap<>();
        map.put(9527,"孙悟空");
        map.put(9528,"唐三藏");//Map的键不允许重复,如果重复,对应的值会被覆盖
        map.put(9528,"猪八戒");//Map的键不允许重复,如果重复,对应的值会被覆盖
        map.put(9530,"沙和尚");
        System.out.println(map);
        //测试常用方法
        //map.clear();清空集合
        System.out.println(map.containsKey(9527));//查看是否包含指定的K
        System.out.println(map.containsValue("沙雕"));//查看是否包含指定的V
        System.out.println(map.equals("孙悟空"));//false,判断集合元素孙悟空与map对象是否相等
        System.out.println(map.get(9528));//根据指定的K来获取对应的V
        System.out.println(map.hashCode());//打印哈希值
        System.out.println(map.isEmpty());//判断集合是否为空
        System.out.println(map.remove(9528));//删除指定的K元素
        System.out.println(map.get(9528));//null，说明映射关系已经被删除
        System.out.println(map);
        System.out.println(map.size());//获取集合元素个数
        Collection<String> str=map.values();//把map集合中的所有value值收集起来放到collection中
        System.out.println(str);//[孙悟空, 沙和尚]
        Collection<String> strings=map.values();//把map集合中的所有value值收集起来放到collection中
        System.out.println(strings);
        //对map集合进行遍历
        //方式一，遍历map中的数据，但是map本身没有迭代器，所以需要转换成set集合
        Set<Integer> keyset=map.keySet();//需要转换成set集合
        System.out.println(keyset);//[9527, 9530]
        Iterator<Integer> iterator=keyset.iterator();//创建迭代器
        while (iterator.hasNext()){
            Integer key=iterator.next();//取出K
            String value=map.get(key);//取出V
            System.out.println("{ "+key+"="+value+" }");
        }
        //方式二，遍历map中的数据,需要把map集合转换成set集合
        // Set<Entry<Integer,String>> entrySet() : 把map集合中的一组key&value数据整体放入set中
        //一对儿 K,V 是一个Entry
        System.out.println("方式二");
        Set<Map.Entry<Integer,String>> entrySet=map.entrySet();//Entry泛型
        Iterator<Map.Entry<Integer,String>> entryIterator=entrySet.iterator();
        while (entryIterator.hasNext()){
            Map.Entry<Integer,String> entry=entryIterator.next();//创建Entry对象
            Integer key=entry.getKey();//取出K
            String value=entry.getValue();//取出V
            System.out.println("{ "+key+"="+value+" }");
        }


    }
}
