package Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/*
对map集合的数据元素进行遍历/迭代
 */
public class MapTestExer {
    public static void main(String[] args) {
        Map<Integer,String> map=new HashMap<>();
        map.put(1001,"张三");
        map.put(1002,"李四");
        map.put(1003,"王五");
        map.put(1004,"傻六");
        //对map集合进行遍历
        //方式一
        //遍历map中的数据,但是map本身没有迭代器,所以需要转换成set集合
        //Set<Key> :把map集合中的所有Key存到set集合中--keySet()*/
        //1.将map中的key取出存入set集合,集合的泛型就是key的类型Integer
        System.out.println("方式一");
        Set<Integer> integerSet=map.keySet();
        Iterator<Integer> iterator=integerSet.iterator();
        while (iterator.hasNext()){
            Integer key=iterator.next();
            String value=map.get(key);
            System.out.println("{ "+key+" = "+value+" }");
        }

        //方式二，遍历map中的数据,需要把map集合转换成set集合
        // Set<Entry<Integer,String>> entrySet() : 把map集合中的一组key&value数据整体放入set中
        //一对儿 K,V 是一个Entry
        System.out.println("方式二");
        Set<Map.Entry<Integer,String>> entrySet=map.entrySet();
        Iterator<Map.Entry<Integer,String>> entryIterator=entrySet.iterator();
        while (entryIterator.hasNext()){
            Map.Entry<Integer,String> entry=entryIterator.next();
            Integer key=entry.getKey();
            String value=entry.getValue();
            System.out.println("{ "+key+" = "+value+" }");
        }
    }
}
