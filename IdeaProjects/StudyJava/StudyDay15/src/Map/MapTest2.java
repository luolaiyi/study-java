package Map;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
本类用于map集合测试
需求：提示并接收用户输入的一串字符，并统计出每个字符出现的次数
 */
public class MapTest2 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入需要统计的字符串内容：");
        String input = scanner.nextLine();
        //3.获取到用户输入的每个字符,String底层维护的是char[]
        //创建map集合存放数据,格式:{b=2,d=4,g=3}
        /**统计的是每个字符出现的次数,所以字符是char类型,次数是int,但是不可以使用基本类型,需要使用包装类型*/
        Map<Character,Integer> map=new HashMap<>();
        //使用for循环结构遍历用户输入的每一个字符
        for (int i=0;i<input.length();i++){
            char key=input.charAt(i);
            //System.out.println(key);
            ////4.统计每个字符出现的个数,存起来,存到map
            Integer value=map.get(key);
            if(value==null){
                map.put(key,1);
            }else {
                map.put(key,value+1);
            }
        }
        System.out.println("各个字符出现的频率是：");
        System.out.println(map);
    }
}
