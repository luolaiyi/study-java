package com.company;

import java.util.Scanner;

public class Test031 {
    public static void main(String[] args) {
        //String str="上海自来水来自海上";
        Scanner input = new Scanner(System.in);

            System.out.println("请用键盘输入一段文字:");
            String integer = input.next();
            int i = integer.length()/2-1;//从字符串中间往左
            int j = integer.length()/2+1;//从字符串中间往右
            boolean c = true;//默认为真
            for(;i >= 0 || j < integer.length();i--,j++){//遍历整个字符串判断是否为回文
                if(integer.charAt(i) != integer.charAt(j))
                    c = false;
                else
                    c = true;
            }
            if(c == false)
                System.out.println(integer+" 不是回文");
            else
                System.out.println(integer+" 是回文");


    }
}
