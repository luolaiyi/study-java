package com.company;

public class ObjectTest {
    public static void main(String[] args) {
        Student stu1= new Student("海绵宝宝",3);
        Student stu2= new Student("海绵宝宝",3);

        System.out.println(stu1);//Student{name='海绵宝宝', age=3}
        System.out.println(stu1.hashCode());//356573597

        System.out.println(stu1.equals(stu2));//底层用了==进行对象间的比较，==比较的是两个对象的地址值，
        // stu1和stu2是两个对象，地址值不同


    }
}

//创建学生类，并定义属性
class Student {
    String name;//姓名
    int age;//年龄

    //构造方法，无参构造
    public Student() {
    }

    //全参构造方法
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //重写toString方法
    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", age=" + age ;

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Student student = (Student) obj;
        return age == student.age &&
               // Objects.equals(name, student.name);
                // public boolean equals(Object obj) { return (this == obj);}
               name==student.name;
    }


}