package com.company;
/*
/*
		 * 编写一个回文字符串，然后调用check方法检查
		 * 该字符串是否为回文，然后输出检查结果。
		 * 若是回文则输出:是回文
		 * 否则输出:不是回文
		 */

import java.util.Scanner;

/**
 * 判读该方法是否是回文
 * @param str 需要判断的字符串
 * @return true表示是回文，false表示不是回文
 **/

public class Test03 {
    public static void main(String[] args) {
        //String str="上海自来水来自海上";
        Scanner input = new Scanner(System.in);
        try{
            System.out.println("请用键盘输入一段文字:");
            String integer = input.next();
            int i = integer.length()/2-1;//从字符串中间往左
            int j = integer.length()/2+1;//从字符串中间往右
            boolean c = true;//默认为真
            for(;i >= 0 || j < integer.length();i--,j++){//遍历整个字符串判断是否为回文
                if(integer.charAt(i) != integer.charAt(j))
                    c = false;
                else
                    c = true;
            }
            if(c == false)
                System.out.println(integer+" 不是回文");
            else
                System.out.println(integer+" 是回文");
        }finally{
            input.close();
        }

    }



}
