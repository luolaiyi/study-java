package com.company;
/*
package day01;
/**
 * 1:输出字符串"HelloWorld"的字符串长度
 * 2:输出"HelloWorld"中"o"的位置
 * 3:输出"HelloWorld"中从下标5出开始第一次出现"o"的位置
 * 4:截取"HelloWorld"中的"Hello"并输出
 * 5:截取"HelloWorld"中的"World"并输出
 * 6:将字符串"  Hello   "中两边的空白去除后输出
 * 7:输出"HelloWorld"中第6个字符"W"
 * 8:输出"HelloWorld"是否是以"h"开头和"ld"结尾的。
 * 9:将"HelloWorld"分别转换为全大写和全小写并输出。

     */
public class Test01 {
    public static void main(String[] args) {

        String str = "HelloWorld";
        System.out.println(str.length());//10
        System.out.println(str.indexOf("o"));//4
        System.out.println(str.lastIndexOf("o"));//6
        System.out.println(str.substring(0,5));//Hello
        System.out.println(str.substring(5,10));//World

        String str1="    Hello     ";
        System.out.println(str1.trim());//Hello
        System.out.println(str.charAt(5));//W
        System.out.println(str.startsWith("H"));//true
        System.out.println(str.endsWith("ld"));//true
        System.out.println(str.toUpperCase());//HELLOWORLD
        System.out.println(str.toLowerCase());//helloworld
    }
}
