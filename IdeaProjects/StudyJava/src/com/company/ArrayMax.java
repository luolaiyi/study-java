package com.company;

public class ArrayMax {
    public static void main(String[] args) {
        int[] array= {10000,99,55,7,66,55,88,44,77,66,999,2000};
        int max=array[0];
        int total=0;
        for(int i=1;i<array.length;i++) {
            if(max<array[i]) {
                max=array[i];
                total=i;
            }
        }
        System.out.println("最大值："+max);
        System.out.println("下标位置："+total);
    }

}
