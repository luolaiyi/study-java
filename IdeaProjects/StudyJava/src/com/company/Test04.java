package com.company;

import java.util.Scanner;

/*
 * 要求用户从控制台输入一个email地址，然后获取该email的用户名(@之前的内容)
 */
public class Test04 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入一个email地址");//1547096263@qq.com
        String str= scanner.next();
        String name=str.substring(0,str.indexOf("@"));
        System.out.println(name);

    }
}
