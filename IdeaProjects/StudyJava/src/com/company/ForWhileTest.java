package com.company;

import java.util.Scanner;

public class ForWhileTest {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int count = 0;    //记录正数个数
        int total = 0;    //记录负数个数
        while (true) {
            int num = scanner.nextInt();
            if (num>0){
                count++;
            }else if(num<0){
                total++;
            } else {
                break;
            }
        }
        System.out.println("正数的个数为："+count);
        System.out.println("负数的个数为："+total);
    }
}
