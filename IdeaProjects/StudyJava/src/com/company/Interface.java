package com.company;
/*
1、接口不能创建对象，一般使用接口实现类的对象来完成功能
2、如果一个类没有明确指定父类，默认父类是object类
3、abstract 可以修饰方法或者类
4、被abstarct修饰的类叫做抽象类,被abstract修饰的方法叫做抽象方法
5、抽象类中可以没有抽象方法
6、如果类中有抽象方法,那么该类必须定义为一个抽象类
7、子类继承了抽象类以后,要么还是一个抽象类,要么就把父类的所有抽象方法都重写
8、多用于多态中
9、抽象类不可以被实例化

 */
public class Interface {
    public static void main(String[] args) {
        interTestImp imp=new interTestImp();
        imp.eat();
        imp.play();

    }
}

/*
1、接口通过关键字interface定义，接口不是类
2、接口没有普通方法，方法默认拼接public abstract
3、接口没有构造方法
4、接口中无成员变量，接口中的是静态常量

 */
interface  interTest{
    public abstract  void eat();
    void  play();

}
/*
1、变成抽象子类
 */

//abstract class  interTestImp implements  interTest{

//}

/*
实现接口中的所有抽象方法
 */
 class  interTestImp implements  interTest{

    @Override
    public void eat() {
        System.out.println("吃东西");
    }

    @Override
    public void play() {
        System.out.println("打游戏");
       

    }
}