package com.company;

public class interfaceView {
    public static void main(String[] args) {

    }
}


interface imp1{
    void save();//保存方法
}

interface imp2{
    void delete();//删除方法
}

/*
1、接口与接口之间是继承关系，可以多继承
 */
interface imp3 extends imp1,imp2{
    //定义接口3继承接口1和接口2

}

/*
类与接口是实现关系，一个类可以实现多个接口
 */
class  imp3teat implements imp3{

    @Override
    public void save() {

    }

    @Override
    public void delete() {

    }
}

class  imp4test implements imp1,imp2{

    @Override
    public void save() {

    }

    @Override
    public void delete() {

    }
}