package com.company;

import java.util.Arrays;

//本类用于测试String方法
public class StringTest {
    public static void main(String[] args) {

        //创建String对象方式1
        char[] value={'a','b','c'};
        String str1= new String(value);
        System.out.println(str1);

        //创建String对象方式2
        String str2="abc";  //堆中常量池
        System.out.println(str2);//效率高
        System.out.println(str1==str2);//false
        System.out.println(str1.equals(str2));//true,string类对equals进行了重写，比较的就是字符串的具体内容

        System.out.println(str2.charAt(1));//b,获取下标为1的char字符
        System.out.println(str2.concat("cccccfg"));//abcdefg,在原有的字符串上进行拼接,注意不改变原来的串
        System.out.println(str2.endsWith("g"));//false,判断是否以指定的元素结尾

        String str3=str2.concat("cccccfg"); //concat拼接，不影响原字符串
        System.out.println(str3.endsWith("g"));
        System.out.println(str3.startsWith("a"));//true,判断是否以指定元素开始
        System.out.println(str3.indexOf("c"));  //   2,字符首个索引出现的位置
        System.out.println(str3.lastIndexOf("c")); // 7,字符最后一个索引出现的位置
        System.out.println(str3.getBytes());//  [B@1540e19d
        System.out.println(str3.length()); //10，数组长度

        String str4="a b c d e f g h i j k";
        System.out.println(Arrays.toString(str4.split(" ")));
        System.out.println(str4.substring(4)); //从下标位置处开始截取剩下的字符串
        System.out.println(str4.substring(1,5));//从下标位置1到5截取字符串[1,5),左闭右开
        System.out.println(str4.toUpperCase()); //把字符串的内容全部转换成大写
        System.out.println(str4.toLowerCase()); //把字符串的内容全部转换成小写

        String str5="     abcde        ";
        System.out.println(str5.trim());  //去除字符串数据前后两头的空格
        System.out.println(String.valueOf(10)+20); //把int10转换成String类型.
        // 1010,不是加法,而是String类型拼接哦
    }

}

