package Bank;

public class CustomerList {
	
	private Customer[] customers;//用来保存客户对象的数组
	private int total = 0                 ;//记录已保存客户对象的数量
	
	//构造器，用来初始化customers数组
    public CustomerList(int totalCustomer) {
    	customers =new Customer[totalCustomer];
		
	}
	//添加用户将参数customer添加到数组中最后一个客户对象记录之后
	public boolean addCustomer(Customer customer) {
		if(total>=customers.length) {
			return false;
		}
	customers[total]=customer;
	total++;
	return true;
		
	}
	//修改用户用参数customer替换数组中由index指定的对象
	public boolean replaceCustomer(int index, Customer cust) {
		if(index<0 || index>=total) {
			return false;
		}
		customers[index]=cust;
		return true;
		
	}
	//删除用户从数组中删除参数index指定索引位置的客户对象记录
	public boolean deleteCustomer(int index) {
		if(index<0 || index>=total) {
			return false;
		}
		for(int i=index;i<total-1;i++) {
			customers[i]=customers[i+1];//后面替换前面
		}
		customers[total-1]=null;//最后一位置空
		total--;//个数减一
		return true;
		
		
	}
	//查看用户返回数组中记录的所有客户对象
	public Customer[] getAllCustomers() {
		Customer[] custtest=new Customer[total];
		for(int i=0;i<total;i++) {
			custtest[i]=customers[i];//将存在的客户数据给新数组
		}
		return custtest;
		
	}
	//查看指定用户返回参数index指定索引位置的客户对象记录
	public Customer getCustomer(int index) {
		if(index<0 || index>=total) {
			return null;
		}
		return customers[index];
		
		
	}
	//获客户个数
	public int getTotal() {
		return total;
		
	}



}
