package Bank;


public class CustomerView {
	private CustomerList customerList = new CustomerList(10);//定义对象customerList
	
	
	
	public CustomerView() {
		Customer cust = new Customer("张三", '男', 30, "010-56253825",
				"abc@email.com");
		customerList.addCustomer(cust); //初始化一个对象-->customers
		
	}
	//显示主菜单，响应用户输入，根据用户操作分别调用其他相应的成员方法（如addNewCustomer），以完成客户信息处理。
	public void enterMainMenu() { 
		
		boolean isFlag=true;//标识符
		while(isFlag) {
			System.out.println("\n-------------------客户信息管理软件-----------------\n");
			System.out.println("                   1 添 加 客 户"                      );
			System.out.println("                   2 修 改 客 户"                      );
			System.out.println("                   3 删 除 客 户"                      );
			System.out.println("                   4 客 户 列 表"                      );
			System.out.println("                   5 退      出\n"                    );
			System.out.print("                     请选择(1-5)："                      );
			
			char menu=CMUtility.readMenuSelection();//读取一个字符
			switch(menu) {
			case '1':
				addNewCustomer();
				break;
			case '2':
				modifyCustomer();
				break;
			case '3':
				deleteCustomer();
				break;
			case '4':
				listAllCustomers();
				break;
			case '5':
				System.out.println("确认是否退出（Y/N）:");
				char isExit=CMUtility.readConfirmSelection();//退出确认
				if (isExit=='Y') {
					isFlag=false;
					
				}
			}
		}
		
		
	}
	//添加客户
	private void addNewCustomer() {
		System.out.println("---------------------添加客户---------------------");
		System.out.print("姓名：");
		String name  = CMUtility.readString(4);
		
		System.out.print("性别：");
		char gender = CMUtility.readChar();
		
		System.out.print("年龄：");
		int age = CMUtility.readInt();
		
		System.out.print("电话：");
		String phone = CMUtility.readString(15);
		
		System.out.print("邮箱：");
		String email = CMUtility.readString(20); 
		
		
		Customer customer=new Customer(name, gender, age, phone, email);//把定义的变量放到customer对象中，全参构造器
	    boolean isTrue =customerList.addCustomer(customer);
	    if (isTrue) {
	    	System.out.println("---------------------添加完成---------------------");
			
		}else {
			System.out.println("----------------客户记录已满,无法添加-----------------");
		}
		
	}
	//修改客户
	private void modifyCustomer() {
		System.out.println("---------------------修改客户---------------------");
		Customer cust;
		int num;
		for(;;) {
					System.out.print("请选择待修改客户编号(-1退出)：");
					 num=CMUtility.readInt();
					if(num==-1) {
						return;
					}
				       cust=customerList.getCustomer(num-1);
					if(cust==null) {
						System.out.println("无法找到指定客户！");
					}else {
						break;
					}

		}
		System.out.print("姓名(" + cust.getName() + ")：");
		String name = CMUtility.readString(4, cust.getName());

		System.out.print("性别(" + cust.getGender() + ")：");
		char gender = CMUtility.readChar(cust.getGender());

		System.out.print("年龄(" + cust.getAge() + ")：");
		int age = CMUtility.readInt(cust.getAge());

		System.out.print("电话(" + cust.getPhone() + ")：");
		String phone = CMUtility.readString(15, cust.getPhone());

		System.out.print("邮箱(" + cust.getEmail() + ")：");
		String email = CMUtility.readString(15, cust.getEmail());
		
		Customer custtemp=new Customer(name, gender, age, phone, email);
		boolean isTrue=customerList.replaceCustomer(num-1, custtemp);
		if(isTrue) {
			System.out.println("---------------------修改完成---------------------");
		}else {
			System.out.println("-------------无法找到指定客户,修改失败----------------");
		}
	}
	//删除客户
	private void deleteCustomer() {
		System.out.println("---------------------删除客户---------------------");

		int index = 0;
		Customer cust = null;
		for (;;) {
			System.out.print("请选择待删除客户编号(-1退出)：");
			index = CMUtility.readInt();
			if (index == -1) {
				return;
			}

			cust = customerList.getCustomer(index - 1);
			if (cust == null) {
				System.out.println("无法找到指定客户！");
			} else
				break;
		}

		System.out.print("确认是否删除(Y/N)：");
		char yn = CMUtility.readConfirmSelection();
		if (yn == 'N')
			return;

		boolean flag = customerList.deleteCustomer(index - 1);
		if (flag) {
			System.out
					.println("---------------------删除完成---------------------");
		} else {
			System.out.println("----------无法找到指定客户,删除失败--------------");
		}
		
	}
	//查看客户
	private void listAllCustomers() {
		System.out.println("---------------------------客户列表---------------------------");
		int tatal=customerList.getTotal();//获取客户个数
		if(tatal==0) {
			System.out.println("没有客户记录");
		}else {
			System.out.println("编号\t姓名\t性别\t年龄\t\t电话\t\t邮箱");
			Customer[] custs=customerList.getAllCustomers();//获取数组中存在的客户信息
			for(int i=0;i<custs.length;i++) {//遍历打印
				System.out.println(i + 1 + "\t" + custs[i].getName() + "\t" +
			custs[i].getGender() + "\t" + custs[i].getAge() + "\t\t" + 
						custs[i].getPhone() + "\t" + custs[i].getEmail());
			}
		}
		
		System.out.println("-------------------------客户列表完成--------------------------");
		
	}
	public static void main(String[] args) {
		CustomerView customerView =new CustomerView();
		customerView.enterMainMenu();//调用菜单界面
		
	}

}
