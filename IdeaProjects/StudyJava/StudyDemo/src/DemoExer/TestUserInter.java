package DemoExer;


/**本类用于进一步测试接口的使用*/
public class TestUserInter {
    //5.创建入口函数main()
    public static void main(String[] args) {
        //6.创建多态对象进行测试
        /**问题:子类创建对象时,默认会调用父类的构造方法
         * 目前接口实现类的父级是一个接口,而接口没有构造方法
         * 那实现类构造方法中的super()调用的是谁呢?
         * 结论1:如果一个类没有明确指定父类,那么默认继承顶级父类Object
         * 所以super()会自动调用Object类中的无参构造
         * */
        /**查看类的继承结构:Ctrl+T*/
        Inter2 i = new Inter2Impl();
        /**结论2:接口中的变量实际上都是静态常量,可以通过类名直接调用*/
        System.out.println(Inter2Impl.age);//通过接口的实现类也可以直接调用
        System.out.println(Inter2.age);//通过接口名可以直接调用
        /**结论3:接口中的变量实际上都是静态常量,值不可以被修改*/
        //Inter2.age = 200;
    }
}

//1.创建接口
interface Inter2{
    /**1.接口中有构造方法吗?--不可以!!!*/
    //2.测试接口是否可以有构造方法
//	public Inter2() {}
    /**2.接口里可以有成员变量吗?--没有!!!
     * 是一个静态常量,实际上的写法是public static final int age = 10;
     * 只不过在接口中可以省略不写
     * */
    int age  = 10;

}

//3.创建接口的实现类
//class Inter2Impl extends Object implements Inter2{
class Inter2Impl implements Inter2{

    //4.创建实现类的构造函数
    public Inter2Impl() {
        super();
        System.out.println("我是Inter2Impl的无参构造");
    }
}
