package DemoExer;
/**本类用于测试接口与类之间的复杂关系*/
public class TestRelation {
    public static void main(String[] args) {
      inter3 inter3=new impInter3();
      inter3.eat();
      inter3.play();
      inter3.sleep();
      inter3.study();
    }
}
interface inter1{
    void eat();
    void sleep();
}
interface inter2{
    void play();
    void study();
}
interface inter3 extends inter1,inter2{

}
 class impInter3 implements inter3{

     @Override
     public void eat() {
         System.out.println("吃饭");
     }

     @Override
     public void sleep() {
         System.out.println("睡觉");
     }

     @Override
     public void play() {
         System.out.println("玩");
     }

     @Override
     public void study() {
         System.out.println("学习");
     }
 }