package DemoExer;
/*
定义接口的实现类
 */
public class ImpInterface implements Interface{
    @Override//对接口中的方法进行重写
    public void eat() {
        System.out.println("每个人都要吃饭");
    }

    @Override//对接口中的方法进行重写
    public void sleep() {
        System.out.println("每个人都要睡觉");

    }

    @Override//对接口中的方法进行重写
    public void play() {
        System.out.println("每个人都喜欢打游戏");

    }
}
