package DemoExer;
/*
冒泡排序
 */
public class Sort {
    public static void main(String[] args) {
        int[] array={1,55,2,88,415,236,44,77,555,954,12,654,71,699,1001,52,1532};
        System.out.println(array.length);

        for (int i=0;i<array.length-1;i++){
            for (int j=0;j<array.length-1-i;j++){
                if (array[j]>array[j+1]){
                    int temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
            }
        }
        System.out.println("数组从小到大的顺序为：");
        for (int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }
        
    }
}
