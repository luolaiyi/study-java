package DemoExer;
/*
对接口的实现类进行测试
 */
public class ImpTest {
    public static void main(String[] args) {
        //Interface i=new Interface()//接口不能创建对象，实例化
        ImpInterface impInterface=new ImpInterface();
        impInterface.eat();
        impInterface.play();
        impInterface.sleep();
        System.out.println("*****************************");

        Interface inter=new ImpInterface();//多态
        inter.eat();
        inter.play();
        inter.sleep();


    }
}
