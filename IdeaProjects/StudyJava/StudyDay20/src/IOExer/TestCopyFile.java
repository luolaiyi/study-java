package IOExer;

import java.io.*;
import java.util.Scanner;

public class TestCopyFile {
    public static void main(String[] args) {
        System.out.println("请输入原文件的目标路径：");
        String f= new Scanner(System.in).nextLine();
        System.out.println("请输入目标文件的路径：");
        String t = new Scanner(System.in).nextLine();

        File from=new File(f);
        File to=new File(t);

        //ZJCopy(from,to);
        ZFCopy(from,to);
    }
    public static void ZJCopy(File from,File to){
        InputStream in = null;
        OutputStream out = null;
        try {
            in=new BufferedInputStream(new FileInputStream(from));
            out=new BufferedOutputStream(new FileOutputStream(to));
            int num;
            while ((num = in.read())!=-1){
                out.write(num);
            }
            System.out.println("文件复制成功");
        }catch (Exception e){
            System.out.println("文件复制失败");
            e.printStackTrace();
        }finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void ZFCopy(File from,File to){
        Reader reader = null;
        Writer writer = null;

        try {
            reader = new BufferedReader(new FileReader(from));
            writer = new BufferedWriter(new FileWriter(to));

            int num;
            while ((num = reader.read())!=-1){
                writer.write(num);
            }
            System.out.println("复制成功");
        }catch (Exception e){
            System.out.println("复制失败");
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
