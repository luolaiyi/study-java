package IOExer;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class InTest {
    public static void main(String[] args) {
       method2();
    }
    public static void method1(){
        try {
            InputStream inputStream = new FileInputStream("D:\\ready\\1.txt");
            int num;
            while ((num =inputStream.read())!=-1){
                System.out.println(num);
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void method2(){
        try {
            InputStream inputStream = new BufferedInputStream(
                    new FileInputStream("D:\\ready\\1.txt"));
            int num;
            while ((num = inputStream.read())!=-1){
                System.out.println(num);
            }
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
