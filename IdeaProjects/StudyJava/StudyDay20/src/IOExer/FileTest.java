package IOExer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileTest {
    public static void main(String[] args) throws IOException {
        File file = new File("D:\\ready\\1.txt");

        System.out.println(file.length());//9,获取指定文件的字节量大小
        System.out.println(file.exists());//true,判断指定文件是否存在
        System.out.println(file.isFile());//true,判断指定内容是否为文件
        System.out.println(file.isDirectory());//false,判断指定内容是否为文件夹
        System.out.println(file.getName());//1.txt,获取指定内容的名字
        System.out.println(file.getParent());//D:\ready,获取指定内容的上级
        System.out.println(file.getAbsolutePath());//D:\ready\1.txt,获取指定内容的绝对路径

        file = new File("D:\\ready\\2.txt");
        /*如果指定创建文件的文件夹不存在,会报错:java.io.IOException
         * 系统找不到指定的路径,由于可能会发生异常,所以调用时需要抛出异常 */
        //在windows中创建不存在的文件2.txt,成功返回true
        System.out.println(file.createNewFile());//true

        file = new File("D:\\ready\\m");
        System.out.println(file.mkdir());//true,创建不存在的单层文件夹m
        file = new File("D:\\ready\\a\\b\\c");
        System.out.println(file.mkdirs());//true,创建不存在的多层文件夹a/b/c

        System.out.println(file.delete());//c被删除,删除文件或者空文件夹
        file  = new File("D:\\ready\\a");
        System.out.println(file.delete());//false,因为a目录里还有b目录
        file  = new File("D:\\ready\\2.txt");
        System.out.println(file.delete());//true,删除2.txt文件成功

        //2.3 文件列表测试
        file = new File("D:\\ready");
        String[] listName = file.list();
        System.out.println(Arrays.toString(listName));

        File[] fs = file.listFiles();
        System.out.println(Arrays.toString(fs));
        System.out.println(fs[0].length());

    }
}
