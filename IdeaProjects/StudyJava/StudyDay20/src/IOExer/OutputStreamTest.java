package IOExer;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamTest {
    public static void main(String[] args) {
      method2();
    }
    public static void method1(){
        OutputStream outputStream=null;
        try {
            outputStream=new FileOutputStream("D:\\ready\\2.txt");
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);
            outputStream.write(100);
            outputStream.write(101);
            outputStream.write(102);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void method2(){
        OutputStream outputStream = null;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream("D:\\ready\\2.txt"));
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);
            outputStream.write(100);
            outputStream.write(101);
            outputStream.write(102);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
