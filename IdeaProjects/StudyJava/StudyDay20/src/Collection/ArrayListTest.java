package Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;

/**本类用于测试ArrayList相关内容*/
public class ArrayListTest {
    public static void main(String[] args) {
        //1.创建对象,使用的是无参构造
        //底层会自动帮我们创建数组存放对象,并且数据的初始容量是10
        ArrayList<Integer> list = new ArrayList();//简写方式

        //2.放入数据进行测试常用方法
        list.add(100);//向集合中添加元素
        list.add(200);//向集合中添加元素
        list.add(300);//向集合中添加元素
        list.add(400);//向集合中添加元素
        list.add(300);//向集合中添加元素
        list.add(200);//向集合中添加元素
        list.add(0,777);//在指定下标处新增元素
        System.out.println(list);
        System.out.println(list.remove(Integer.valueOf(300)));//true
        System.out.println(list);
        System.out.println(list.remove(Integer.valueOf(300)));//true
        System.out.println(list);
        System.out.println(list.remove(Integer.valueOf(300)));//true
        System.out.println(list);

        //list.clear();//清空集合
        //System.out.println(list);

        System.out.println(list.contains(300));//true,判断集合是否包含元素300
        System.out.println(list.get(0));//777,获取集合中指定下标位置上的元素
        System.out.println(list.indexOf(200));//2,判断集合中指定元素第一次出现的下标
        System.out.println(list.lastIndexOf(200));//6,判断集合中指定元素最后一次出现的下标
        System.out.println(list.isEmpty());//false,判断集合是否为空
        System.out.println(list.remove(1));//100,移除集合中指定下标对应着的元素,移除成功,返回被移除的元素
        /**
         *Exception in thread "main" java.lang.IndexOutOfBoundsException: Index: 300, Size: 6
         *这是根据下标来删除元素的,而此集合没有下标300,最大下标为6,所以会数组下标越界
         *System.out.println(list.remove(300));--错误
         *如果想根据具体的元素内容移除元素,需要先把int类型的数据转成Integer数据类型
         * * */
        System.out.println(list.remove(Integer.valueOf(300)));//true
        System.out.println(list);
        System.out.println(list.set(2, 77));//更改集合中对应下标上元素的值
        System.out.println(list);
        System.out.println(list.size());
        System.out.println(Arrays.toString(list.toArray()));//将集合元素存入数组,打印数组的具体值

        //4种迭代方式
        //方式1:for循环
        System.out.println("方式1");
        //开始:0   结束:最大下标(集合长度-1)  变化:++
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));//根据下标获取对应下标位置上的元素
        }
        //方式2:增强for循环
        System.out.println("方式2");
        for (Integer i : list) {//遍历list集合,每次循环得到的元素是Integer类型的i
            System.out.println(i);//打印每次循环到的集合元素
        }
        //方式3:Iterator
        /**1.获取迭代器  2.判断是否还有元素(一般用来做循环条件)  3.获取当前遍历到的元素*/
        System.out.println("方式3");
        Iterator<Integer> it = list.iterator();//获取集合用来迭代的迭代器,此迭代器是继承自Collection接口中的
        while(it.hasNext()) {//通过迭代器来判断集合中是否还有元素,如果有,继续迭代,如果没有,结束循环
            Integer num = it.next();//获取当前遍历到的集合元素
            System.out.println(num);//打印当前遍历到的集合元素
        }
        //方式4:ListIterator
        System.out.println("方式4");
        ListIterator<Integer> it2 = list.listIterator();//获取集合用来迭代的迭代器,此迭代器是List接口中的迭代器
        while(it2.hasNext()) {//通过迭代器来判断集合中是否还有元素,如果有继续迭代,如果没有,结束循环
            Integer s2 = it2.next();//获取当前遍历到的集合元素
            System.out.println(s2);//打印当前遍历到的集合元素
        }
    }
}
