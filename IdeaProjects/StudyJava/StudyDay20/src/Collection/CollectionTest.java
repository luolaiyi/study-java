package Collection;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**本类用来测试Collection接口*/
public class CollectionTest {
    public static void main(String[] args) {
        //1.创建Collection接口相关对象
        //Collection c = new Collection();//报错,因为Collection是接口不能实例化new
        /**1.<Integer>是泛型,用来约束集合中的元素的类型,只能写引用类型,不能是基本类型*/
        Collection<Integer> c = new ArrayList<Integer>();

        //2.1测试常用方法--对于单个集合的方法
        c.add(100);//向集合中添加元素
        c.add(200);//向集合中添加元素
        c.add(300);//向集合中添加元素
        c.add(400);//向集合中添加元素
        c.add(500);//向集合中添加元素
        System.out.println(c);//直接打印查看集合中的元素

        //c.clear();//清空集合中的元素
        //System.out.println(c);

        System.out.println( c.contains(300) );//true,判断集合中是否包含元素300
        System.out.println( c.hashCode() );//127240651,返回集合对应的哈希码值
        System.out.println( c.isEmpty() );//false,判断集合是否为空
        System.out.println( c.remove(100) );//true,移出集合中的元素100,移出成功返回true
        System.out.println( c );//[200, 300, 400, 500],100被成功移除
        System.out.println( c.size() );//4,获取集合的元素个数/类似数组长度
        System.out.println( c.equals(200) );//false,判断是否与100相等

        /**接返回值类型快捷键:Shift+alt+L*/
        Object[] array = c.toArray();//把集合中的元素放入数组
        System.out.println(Arrays.toString(array));//使用数组的工具类查看数组中的元素内容

        //2.2测试常用方法--集合间的操作
        Collection<Integer> c2 = new ArrayList<Integer>();

        c2.add(2);//给c2集合添加元素
        c2.add(4);//给c2集合添加元素
        c2.add(6);//给c2集合添加元素
        System.out.println(c2);//[2, 4, 6],直接打印查看c2集合的内容

        c.addAll(c2);//把c2集合添加到c集合中
        System.out.println(c);//[200, 300, 400, 500, 2, 4, 6],追加操作
        System.out.println(c.contains(c2));//false
        System.out.println(c.containsAll(c2));//true,查看c集合是否包含c2集合中的所有元素

        System.out.println(c.removeAll(c2));//true,删除c集合中属于c2集合的所有元素
        System.out.println(c);//[200, 300, 400, 500],查看c集合删除c2集合后的结果,正确删除
        //System.out.println(c.retainAll(c2));//true,删除c集合
        //System.out.println(c);//[]

        //2.3 用来遍历/迭代集合中的元素 Iterator<E> iterator()
        /**
         * 1.如何获取迭代器 c.iterator()
         * 2.判断集合是否有下个元素 it.hasNext()
         * 3.获取当前迭代到的元素 it.next()
         */
        Iterator<Integer> it = c.iterator();
        //通过iterator迭代器,循环获取集合中的元素
        while(it.hasNext()) {
            //hasNext()用来判断集合中是否有下个元素,有就返回true,继续循环取值
            Integer num = it.next();//next()用来获取迭代到的元素
            System.out.println(num);
        }

    }
}

