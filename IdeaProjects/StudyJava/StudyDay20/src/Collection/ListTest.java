package Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**本类用于测试List接口*/
public class ListTest {
    public static void main(String[] args) {
        //1.创建List接口对象,注意此处创建的多态对象,List是接口,不能直接实例化
        List<String> list = new ArrayList<String>();//注意导包:java.util...导包快捷键:Ctrl+Shift+O

        //2.测试继承自Colletion接口的方法
        list.add("大力娃");//向集合中添加元素
        list.add("千顺娃");
        list.add("头铁娃");
        list.add("喷火娃");
        list.add("喷水娃");
        list.add("隐身娃");
        list.add("小紫娃");
        System.out.println(list);//查看集合中的所有元素
        //list.clear();//清空集合
        //System.out.println(list);//[],集合已清空

        System.out.println("**********我是一个无情的分界线**************");
        System.out.println( list.contains("大力娃"));//判断是否包含元素"大哥"
        System.out.println( list.equals("头铁娃"));//判断list对象是否与"二哥"相等
        System.out.println( list.hashCode());//获取集合的哈希码值
        System.out.println( list.isEmpty());//判断集合是否为空
        System.out.println( list.remove("喷水娃"));//移除集合中的元素"五哥"
        System.out.println( list );//查看集合中的内容
        System.out.println( list.size());//获取集合的元素个数,类似于数组的长度
        //将集合中的元素存入数组中,打印需要使用数组的工具类Arrays
        System.out.println( Arrays.toString(list.toArray()));//[大哥, 二哥, 三哥, 四哥, 六哥, 七弟]

        //3.List接口的特有方法  -- 都是可以根据索引来操作的方式
        list.add("小蝴蝶");//追加在最后
        System.out.println(list);
        list.add(1,"蝎子精");//在指定索引处添加指定的元素
        System.out.println(list);
        System.out.println(list.get(2));//获取指定下标对应的元素
        list.add(3,"小蝴蝶");
        System.out.println(list);
        System.out.println(list.indexOf("小蝴蝶"));//获取指定元素第一次出现的索引
        System.out.println(list.lastIndexOf("小蝴蝶"));//获取指定元素最后一次出现的索引
        System.out.println(list.remove(6));//移除指定索引的元素
        System.out.println(list.set(0, "妖精蛇"));//重置指定索引处位置的值
        System.out.println(list);
        List<String> subList = list.subList(2, 6);//截取子串,[2,6)含头不含尾
        System.out.println(subList);

        //4.集合间的操作
        List<String> list2 = new ArrayList<String>();
        list2.add("1");
        list2.add("2");
        list2.add("3");
        System.out.println( list.addAll(list2) );//把list2集合添加到list集合中
        System.out.println( list.addAll(1,list2) );//把list2集合添加到list集合的指定位置处
        System.out.println( list );
        System.out.println( list.contains(list2) );//判断list集合是否有一个元素是list2
        System.out.println( list.containsAll(list2) );//判断list集合是否包含list2集合中的所有元素
        System.out.println( list.removeAll(list2) );//删除list集合中list2集合中的所有元素
        System.out.println( list);
    }
}
