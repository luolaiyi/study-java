package Collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/*
List集合四种遍历方法
 */
public class ListFor {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("张");
        list.add("关");
        list.add("赵");
        list.add("马");
        list.add("黄");

        System.out.println("方式一");//普通For循环
        for (int i=0;i<list.size();i++){
            String str= list.get(i);
            System.out.println(str);
        }

        System.out.println("方式二");//增强For循环
        for (String str:list){
            System.out.println(str);
        }

        System.out.println("方式三");//iterator() 是继承自父接口Collection的
        Iterator<String> iterator=list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("方式四");//listIterator()是List接口特有的
        ListIterator<String> listIterator =list.listIterator();
        while (listIterator.hasNext()){
            System.out.println(listIterator.next());
        }
    }
}
