package JdbcExer;

import java.sql.*;

public class Exer3 {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/cgb";
        String user = "root";
        String pwd = "root";

        try {
            Connection connection = DriverManager.getConnection(url,user,pwd);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from emp");
            while (resultSet.next()){
                for(int i=1;i<=5;i++){
                    System.out.println(resultSet.getString(i));
                }
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
