package JdbcExer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/*
jdbc练习
 */
public class Exer1 {
    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/cgb";
        String user = "root";
        String pwd = "root";
        Connection connection = DriverManager.getConnection(url,user,pwd);
        //Connection connection = DriverManager.getConnection(url,user,pwd);

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from student");
        while (resultSet.next()){
            for (int i=1; i<=5;i++){
                System.out.println(resultSet.getString(i));
            }
        }
        resultSet.close();
        statement.close();
        connection.close();
    }
}
