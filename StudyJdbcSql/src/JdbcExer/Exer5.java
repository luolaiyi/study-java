package JdbcExer;

import java.sql.*;
import java.util.Scanner;

public class Exer5 {
    public static void main(String[] args) {
        //method1();
        method2();

    }
    public static void method1(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/cgb";
        String user = "root";
        String pwd = "root";

        try {
            Connection connection = DriverManager.getConnection(url,user,pwd);
            Statement statement = connection.createStatement();
             int num = statement.executeUpdate("insert  into usertest values ('3','stark','888888')");
            System.out.println(num);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void method2(){
        System.out.println("请输入用户名:");
        String name = new Scanner(System.in).nextLine();
        System.out.println("请输入用户名密码:");
        String password = new Scanner(System.in).nextLine();

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url="jdbc:mysql://localhost:3306/cgb";
        String user ="root";
        String pwd = "root";
        try {
            Connection connection =  DriverManager.getConnection(url,user,pwd);
            String sql = "select * from usertest where name=? and password=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //ResultSet resultSet =preparedStatement.executeQuery();
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                System.out.println("登录成功");
            }else {
                System.out.println("登录失败");
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
}
