package JdbcExer;

import java.sql.*;
import java.util.Scanner;

public class Exer7 {
    public static void main(String[] args) {
        method();
    }

    public static void method(){

        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入用户名");
        String name = scanner.nextLine();
        System.out.println("请输入用户名密码");
        String password = scanner.nextLine();


        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/cgb";
            String user="root";
            String paw="root";

            Connection connection = null;
            PreparedStatement pst = null;
            ResultSet resultSet = null;
            try {
                connection = DriverManager.getConnection(url,user,paw);


                //Statement statement = connection.createStatement();
                //jack'#

                String sql="select * from usertest where name=? and password=?";
                pst=connection.prepareStatement(sql);

                pst.setString(1,name);
                pst.setString(2,password);

                resultSet = pst.executeQuery();

                if (resultSet.next()){
                    System.out.println("登录成功");
                }else {
                    System.out.println("登录失败");
                }
                //resultSet.close();
                //pst.close();
                //connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }finally {
                JDBCUtils.close(resultSet,pst,connection);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }

    }

}
