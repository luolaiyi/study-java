package Jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

/*
测试JDBC
 */
public class Test1 {
    public static void main(String[] args) throws Exception {
        //Class.forName("com.mysql.jdbc.Driver");//获取字节码对象

        String url="jdbc:mysql://localhost:3306/cgb";
        //简写String url="jdbc:mysql:///cgb";//localhost:3306
        String user="root";
        String pwd="root";
        Connection connection = DriverManager.getConnection(url,user,pwd);

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from student");

        while (resultSet.next()){
            String Sno= resultSet.getString("Sno");
            String Sname= resultSet.getString("Sname");
            String Ssex= resultSet.getString("Ssex");
            Date Sbirthday= resultSet.getDate("Sbirthday");
            String Class= resultSet.getString("Class");
            System.out.println("学号:"+Sno+",姓名:"+Sname+",性别:"+Ssex+",生日:"+Sbirthday+",班级:"+Class);
        }
        resultSet.close();
        statement.close();
        connection.close();
    }
}