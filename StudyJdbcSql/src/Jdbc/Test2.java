package Jdbc;

import java.sql.*;
import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) throws Exception {
        //method1();
        //login();
        loginDemo();
    }


    public static void method1() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/cgb";
        String user = "root";
        String pwd = "root";

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, pwd);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        int row = 0;
        try {
            row = statement.executeUpdate("insert  into dept values ('7','jerry','火星')");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println(row);
        try {
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

//    public static void login(){
//        //根据用户名和密码
//        Scanner scanner=new Scanner(System.in);
//        System.out.println("请输入用户名");
//        String name = scanner.nextLine();
//        System.out.println("请输入用户名密码");
//        String password = scanner.nextLine();
//
//
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            String url="jdbc:mysql://localhost:3306/cgb";
//            String user="root";
//            String paw="root";
//            try {
//                Connection connection = DriverManager.getConnection(url,user,paw);
//
//
//                Statement statement = connection.createStatement();
//                //jack'#
//                ResultSet resultSet =
//                        statement.executeQuery("select * from usertest where name='"+name+"' and password='"+password+"'");
//                ;

//                if (resultSet.next()){
//                    System.out.println("登录成功");
//                }else {
//                    System.out.println("登录失败");
//                }
//            } catch (SQLException throwables) {
//                throwables.printStackTrace();
//            }
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    public static void loginDemo(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入用户名");
        String name = scanner.nextLine();
        System.out.println("请输入用户名密码");
        String password = scanner.nextLine();


        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/cgb";
            String user="root";
            String paw="root";
            try {
                Connection connection = DriverManager.getConnection(url,user,paw);


                //Statement statement = connection.createStatement();
                //jack'#

                String sql="select * from usertest where name=? and password=?";
                PreparedStatement pst=connection.prepareStatement(sql);

                pst.setString(1,name);
                pst.setString(2,password);

                ResultSet resultSet = pst.executeQuery();

                if (resultSet.next()){
                    System.out.println("登录成功");
                }else {
                    System.out.println("登录失败");
                }
                resultSet.close();
                pst.close();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }
    }
}
